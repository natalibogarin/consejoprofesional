$(document).ready(function(){
  function initToolbarBootstrapBindings() {
    var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
    'Times New Roman', 'Verdana'],
    fontTarget = $('[title=Fuente]').siblings('.dropdown-menu');
    $.each(fonts, function (idx, fontName) {
      fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
    });
    $('a[title]').tooltip({container:'body'});
    $('.dropdown-menu input').click(function() {return false;})
      .change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
      .keydown('esc', function () {this.value='';$(this).change();});

    $('[data-role=magic-overlay]').each(function () { 
      var overlay = $(this), target = $(overlay.data('target')); 
      overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
    });
    if ("onwebkitspeechchange"  in document.createElement("input")) {
      var editorOffset = $('#editor').offset();
      $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
    } else {
      $('#voiceBtn').hide();
    }
  };

  function showErrorAlert (reason, detail) {
    var msg='';
    if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
    else {
      console.log("error uploading file", reason, detail);
    }
    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
      '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
    };
    initToolbarBootstrapBindings();
    $('#editor').wysiwyg({ fileUploadError: showErrorAlert} );
    window.prettyPrint && prettyPrint();
  }
);

$(document).ready(function(){
	$("#bottonSubmit").click(function(event) {
		$.ajax({
		  	url: urlNovedad,
		  	type: 'POST',
		  	dataType: 'json',
		  	data: {
		  		html: $("#editor").html(),
		  		consejoId: $("#consejoId").val(),
		  		seccion: $("#seccion").val(),
          extra: $("#novedadimghidden").val(),
		  	}
	  	})
	  	.always(function(data) {
			$("#consejoId").attr("value",data.transaccion);
		});
	});
});


$(document).ready(function(){
  /************ NOVEDADES LOGIC *******************/
  $("#novedadimg").AjaxFileUpload({
    action: urlUpload,
    onComplete: function(filename, response) {
      $("#novedadimghidden").val(response.msg);
      $("#uploads").html(
        $("<img />").attr("src", response.msg).attr("width", 200)
      );
    }
  });

  $("#bottonSubmitNovedad").click(function(event) {
    $.ajax({
        url: urlNovedad,
        type: 'POST',
        dataType: 'json',
        data: {
          novedadcontenido: $("#editor").html(),
          novedadresumen: $("#novedadresumen").val(),
          novedadtitulo: $("#novedadtitulo").val(),
          novedadimghidden: $("#novedadimghidden").val(),
          homeslide: $("#homeslide").val(),
          novedadid: $("#novedadid").val()
        }
      })
      .always(function(data) {
        if (data.action) {
          window.location.href = data.action;
        };
    });
  });
  /************ CURSOS LOGIC *******************/
  $("#cursolink").AjaxFileUpload({
    action: urlUpload,
    onComplete: function(filename, response) {
      $("#cursolinkhidden").val(response.msg);
      $("#uploads").html(
        $("<img />").attr("src", response.msg).attr("width", 200)
      );
    }
  });

  $("#bottonSubmitCurso").click(function(event) {
    $.ajax({
        url: urlNovedad,
        type: 'POST',
        dataType: 'json',
        data: {
          "cursoNombre" : $("#cursoNombre").val(),
          "cursoduracion" : $("#cursoduracion").val(),
          "cursoprograma" : $("#cursoprograma").val(),
          "cursodonde" : $("#cursodonde").val(),
          "cursodetalle" : $("#editor").html(),
          "cursolink" : $("#cursolinkhidden").val(),
          "cursoId": $("#cursoId").val()
        }
      })
      .always(function(data) {
        if (data.action) {
          window.location.href = data.action;
        };
    });
  });
});
