<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leyes_model extends CI_Model {

	public function newLey($ley)
	{
		try {
			$this->db->insert('ley', $ley);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteLey($idLey)
	{
		try {
			$this->db->where('idLey', $idLey);
			$this->db->delete('ley');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateLey($ley)
	{
		try {
			$this->db->where('ley', $ley['idLey']);
			$this->db->update('ley', $ley);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getLey($idLey)
	{
		try {
			$this->db->get('ley', 10, 0);
			return $this->db->result();
		} catch (Exception $e) {
			return false;
		}
	}

}

/* End of file Leyes_model.php */
/* Location: ./application/models/Leyes_model.php */