<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consejo_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function newConsejoInfo($infoConsejo)
	{
		try {
			$this->db->insert('infoConsejo', $infoConsejo);
			$id = $this->db->insert_id();
			return $id;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteConsejoInfo($consejoId)
	{
		try {
			$this->db->where('consejoId', $consejoId);
			$this->db->delete('infoConsejo');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateConsejoInfo($infoConsejo)
	{
		try {
			$this->db->where('consejoId', $infoConsejo['consejoid']);
			$this->db->update('infoConsejo', $infoConsejo);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getConsejoInfo($consejoId)
	{
		try {
			$this->db->get('infoConsejo', 10, 0);
			return $this->db->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getSeccion($seccion)
	{
		try {
			$this->db->where('seccion =', $seccion);
			return $this->db->get('infoConsejo')->result();
		} catch (Exception $e) {
			return false;
		}
	}
}

/* End of file Consejo_model.php */
/* Location: ./application/models/Consejo_model.php */