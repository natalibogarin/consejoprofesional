<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autoridades_model extends CI_Model {

	public function newAutoridad($profesional)
	{
		try {
			$this->db->insert('profesional', $profesional);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteAutoridad($idAutoridad)
	{
		try {
			$this->db->where('profesionalid', $idAutoridad);
			$this->db->delete('profesional');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateAutoridad($profesional)
	{
		try {
			$this->db->where('profesionalid', $profesional['profesionalid']);
			$this->db->update('profesional', $profesional);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getAutoridad($idAutoridad)
	{
		try {
			$this->db->select('*');
			$this->db->join('cargo', 'profesional.cargoid=cargo.cargoid');
			$this->db->where('profesional.profesionalid', (int)$idAutoridad);
			return $this->db->get('profesional')->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getCargos()
	{
		try {
			return $this->db->get('cargo')->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getTotalRows()
	{
		try {
			$this->db->select('profesional.profesionalid, profesional.profesionalnombre, profesional.profesionaltitulo, cargo.cargonombre');
			$this->db->join('cargo', 'profesional.cargoid=cargo.cargoid');
			$query = $this->db->get('profesional');
			return $query->num_rows();
		} catch (Exception $e) {
			return 0;
		}
	}

	public function getAutoridadesPerPage($from, $to)
	{
		try {
			$this->db->select('profesional.profesionalid, profesional.profesionalnombre, profesional.profesionaltitulo, cargo.cargonombre');
			$this->db->join('cargo', 'profesional.cargoid=cargo.cargoid');
			return $this->db->get('profesional', $from, $to)->result();
		} catch (Exception $e) {
			return 0;
		}
	}
}

/* End of file Autoridades_model.php */
/* Location: ./application/models/Autoridades_model.php */