<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enlaces_model extends CI_Model {

	public function newEnlace($enlace)
	{
		try {
			$this->db->insert('enlace', $enlace);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteEnlace($idEnlace)
	{
		try {
			$this->db->where('idEnlace', $idEnlace);
			$this->db->delete('enlace');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateEnlace($enlace)
	{
		try {
			$this->db->where('enlace', $enlace['idEnlace']);
			$this->db->update('enlace', $enlace);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getEnlace($idEnlace)
	{
		try {
			$this->db->get('enlace', 10, 0);
			return $this->db->result();
		} catch (Exception $e) {
			return false;
		}
	}

}

/* End of file Enlaces_model.php */
/* Location: ./application/models/Enlaces_model.php */