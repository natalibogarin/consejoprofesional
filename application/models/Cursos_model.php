<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos_model extends CI_Model {

	public function newCurso($curso)
	{
		try {
			$this->db->insert('cursos', $curso);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteCurso($cursoId)
	{
		try {
			$this->db->where('cursoId', $cursoId);
			$this->db->delete('cursos');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateCurso($curso)
	{
		try {
			$this->db->where('cursoId', $curso['cursoId']);
			$this->db->update('cursos', $curso);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getCurso($cursoId)
	{
		try {
			return $this->db->get_where('cursos', array('cursoId' => $cursoId))->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getCursos($tcid='')
	{
		try {
			return $this->db->get_where('cursos', array('tcid' => $tcid))->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getTotalRows($tcid='')
	{
		try {
			$query = $this->db->get_where('cursos', array('tcid' => $tcid));
			return $query->num_rows();
		} catch (Exception $e) {
			return false;
		}
	}

}

/* End of file Cursos_model.php */
/* Location: ./application/models/Cursos_model.php */