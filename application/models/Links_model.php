<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Links_model extends CI_Model {

	public function newLink($curso)
	{
		try {
			$this->db->insert('links', $curso);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteLink($linkid)
	{
		try {
			$this->db->where('linkid', $linkid);
			$this->db->delete('links');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateLink($curso)
	{
		try {
			$this->db->where('linkid', $curso['linkid']);
			$this->db->update('links', $curso);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getLink($linkid)
	{
		try {
			return $this->db->get_where('links', array('linkid' => $linkid))->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getLinks($tlid='')
	{
		try {
			$this->db->order_by("linkfecha", "desc"); 
			return $this->db->get_where('links', array('tlid' => $tlid))->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getTotalRows($tlid='')
	{
		try {
			$query = $this->db->get_where('links', array('tlid' => $tlid));
			return $query->num_rows();
		} catch (Exception $e) {
			return false;
		}
	}

}

/* End of file Links_model.php */
/* Location: ./application/models/Links_model.php */