<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	public function newUser($user)
	{
		try {
			$this->db->insert('user', $user);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteUser($idConferencia)
	{
		try {
			$this->db->where('idConferencia', $idConferencia);
			$this->db->delete('user');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateUser($user)
	{
		try {
			$this->db->where('user', $user['idConferencia']);
			$this->db->update('user', $user);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getUser($idConferencia)
	{
		try {
			$this->db->get('user', 10, 0);
			return $this->db->result();
		} catch (Exception $e) {
			return false;
		}
	}

}

/* End of file Users_model.php */
/* Location: ./application/models/Users_model.php */