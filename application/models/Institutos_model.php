<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institutos_model extends CI_Model {

	public function newInstituto($instituciones)
	{
		try {
			$this->db->insert('instituciones', $instituciones);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteInstituto($institutoId)
	{
		try {
			$this->db->where('institutoId', $institutoId);
			$this->db->delete('instituciones');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateInstituto($instituciones)
	{
		try {
			$this->db->where('instituciones.institutoId', $instituciones['institutoId']);
			$this->db->update('instituciones', $instituciones);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getInstituto($institutoId)
	{
		try {
			return $this->db->get('instituciones')->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getTotalRows()
	{
		try {
			$query = $this->db->get('instituciones');
			return $query->num_rows();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getInstitutosPerPage($from, $to)
	{
		try {
			return $this->db->get('instituciones', $from, $to)->result();
		} catch (Exception $e) {
			return false;
		}
	}
}

/* End of file Institutos_model.php */
/* Location: ./application/models/Institutos_model.php */