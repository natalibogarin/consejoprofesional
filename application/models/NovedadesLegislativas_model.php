<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NovedadesLegislativas_model extends CI_Model {

	public function newNovedadLegislativa($novedad)
	{
		try {
			$this->db->insert('novedad', $novedad);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteNovedadLegislativa($idNovedad)
	{
		try {
			$this->db->where('idNovedad', $idNovedad);
			$this->db->delete('novedad');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateNovedadLegislativa($novedad)
	{
		try {
			$this->db->where('novedad', $novedad['idNovedad']);
			$this->db->update('novedad', $novedad);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getNovedadLegislativa($idNovedad)
	{
		try {
			$this->db->get('novedad', 10, 0);
			return $this->db->result();
		} catch (Exception $e) {
			return false;
		}
	}

}

/* End of file NovedadesLegislastivas_model.php */
/* Location: ./application/models/NovedadesLegislastivas_model.php */