<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Novedades_model extends CI_Model {

	public function newNovedad($novedad)
	{
		$novedad["novedadhtml"] = str_replace(" ", "-", $novedad["novedadtitulo"]);
		try {
			$this->db->insert('novedades', $novedad);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteNovedad($novedadid)
	{
		try {
			$this->db->where('novedadid', $novedadid);
			$this->db->delete('novedades');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateNovedad($novedad)
	{
		try {
			$this->db->where('novedadid', $novedad['novedadid']);
			$this->db->update('novedades', $novedad);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getNovedad($novedadid)
	{
		try {
			return $this->db->get_where('novedades', array('novedadid' => $novedadid))->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getNovedades($tipo='', $from, $to)
	{
		try {
			$this->db->order_by("novedadfecha", "desc"); 
			return $this->db->get_where('novedades', array('tnid' => $tipo), $from, $to)->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getNovedadesHome($tipo='')
	{
		try {
			$this->db->limit('6');
			$this->db->order_by("novedadfecha", "desc"); 
			return $this->db->get_where('novedades', array('tnid' => $tipo))->result();
		} catch (Exception $e) {
			return false;
		}
	}
	public function getTotalRows($tipo='')
	{
		try {
			$query = $this->db->get_where('novedades', array('tnid' => $tipo))->num_rows();
			return $query;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getNovedadesSlide($tipo='')
	{
		try {
			$this->db->order_by("novedadfecha", "desc"); 
			return $this->db->get_where('novedades', array('tnid' => $tipo, 'homeslide'=>'si'))->result();
		} catch (Exception $e) {
			return false;
		}
	}

	public function getNovedadSlug($slug='')
	{
		try {
			return $this->db->get_where('novedades', array('novedadhtml' => $slug))->result();
		} catch (Exception $e) {
			return false;
		}
	}
}

/* End of file Novedades_model.php */
/* Location: ./application/models/Novedades_model.php */