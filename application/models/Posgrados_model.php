<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posgrados_model extends CI_Model {

	public function newPosgrado($posgrado)
	{
		try {
			$this->db->insert('posgrado', $posgrado);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deletePosgrado($idPosgrado)
	{
		try {
			$this->db->where('idPosgrado', $idPosgrado);
			$this->db->delete('posgrado');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updatePosgrado($posgrado)
	{
		try {
			$this->db->where('posgrado', $posgrado['idPosgrado']);
			$this->db->update('posgrado', $posgrado);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getPosgrado($idPosgrado)
	{
		try {
			$this->db->get('posgrado', 10, 0);
			return $this->db->result();
		} catch (Exception $e) {
			return false;
		}
	}

}

/* End of file Posgrados_model.php */
/* Location: ./application/models/Posgrados_model.php */