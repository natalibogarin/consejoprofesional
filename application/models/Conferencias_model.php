<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conferencias_model extends CI_Model {

	public function newConferencia($conferencia)
	{
		try {
			$this->db->insert('conferencia', $conferencia);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteConferencia($idConferencia)
	{
		try {
			$this->db->where('idConferencia', $idConferencia);
			$this->db->delete('conferencia');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateConferencia($conferencia)
	{
		try {
			$this->db->where('conferencia', $conferencia['idConferencia']);
			$this->db->update('conferencia', $conferencia);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getConferencia($idConferencia)
	{
		try {
			$this->db->get('conferencia', 10, 0);
			return $this->db->result();
		} catch (Exception $e) {
			return false;
		}
	}

}

/* End of file Conferencias_model.php */
/* Location: ./application/models/Conferencias_model.php */