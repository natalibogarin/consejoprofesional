<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JovenesAbogados_model extends CI_Model {

	public function newNovedad($novedad)
	{
		try {
			$this->db->insert('novedad', $novedad);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function deleteNovedad($idNovedad)
	{
		try {
			$this->db->where('idNovedad', $idNovedad);
			$this->db->delete('novedad');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function updateNovedad($novedad)
	{
		try {
			$this->db->where('novedad', $novedad['idNovedad']);
			$this->db->update('novedad', $novedad);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getNovedad($idNovedad)
	{
		try {
			$this->db->get('novedad', 10, 0);
			return $this->db->result();
		} catch (Exception $e) {
			return false;
		}
	}

}

/* End of file JovenesAbogados_model.php */
/* Location: ./application/models/JovenesAbogados_model.php */