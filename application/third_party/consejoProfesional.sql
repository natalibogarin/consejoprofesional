-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 28, 2015 at 04:03 AM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `consejoProfesional`
--

-- --------------------------------------------------------

--
-- Table structure for table `cargo`
--

CREATE TABLE IF NOT EXISTS `cargo` (
  `cargoid` int(11) NOT NULL AUTO_INCREMENT,
  `cargonombre` varchar(100) NOT NULL,
  `cargodesc` text NOT NULL,
  PRIMARY KEY (`cargoid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `cargo`
--

INSERT INTO `cargo` (`cargoid`, `cargonombre`, `cargodesc`) VALUES
(1, 'PRESIDENTE', ''),
(2, 'VICE PRESIDENTE', ''),
(3, 'SECRETARIO', ''),
(4, 'PRO SECRETARIO', ''),
(5, 'TESORERO', ''),
(6, 'PRO TESORERO', ''),
(7, 'VOCALES TITULARES', ''),
(8, 'VOCALES SUPLENTES', ''),
(9, 'REVISORES DE CUENTA TITULARES', ''),
(10, 'REVISORES DE CUENTA SUPLENTES', ''),
(11, 'REPRESENTANTES TITULARES ANTE LA FACA', ''),
(12, 'REPRESENTANTES SUPLENTES ANTE LA FACA', '');

-- --------------------------------------------------------

--
-- Table structure for table `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `cursoId` int(11) NOT NULL AUTO_INCREMENT,
  `cursoNombre` varchar(100) NOT NULL,
  `cursoduracion` varchar(100) NOT NULL,
  `cursodonde` varchar(100) NOT NULL,
  `cursodetalle` text NOT NULL,
  `cursoprograma` text,
  `cursolink` text NOT NULL,
  `tcid` int(11) NOT NULL,
  PRIMARY KEY (`cursoId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cursos`
--

INSERT INTO `cursos` (`cursoId`, `cursoNombre`, `cursoduracion`, `cursodonde`, `cursodetalle`, `cursoprograma`, `cursolink`, `tcid`) VALUES
(1, 'curso de algo', '3 semanas', 'se realizara en algun lado', '\n            \n            asdas<div>asdasda</div><div><b>asdasda</b></div><div>asdasdas</div><div><i>asdasdas</i></div>                    ', 'https://www.youtube.com/watch?v=09R8_2nJtjg', 'http://localhost/consejoprofesional/assetics/img/cursos/20151209160054.jpg', 2),
(2, 'masdasda', 'adasda', 'asdasdas', '<blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;"><blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;">sdfsadasdasdasadas</blockquote><blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;">asdasda</blockquote><blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;"><br></blockquote><blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;">asdasda</blockquote><blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;">asdas</blockquote></blockquote>', NULL, 'http://localhost/consejoprofesional/assetics/img/cursos/1005548_943752262326933_3017408031362224263_', 2),
(3, 'curso de prueba', 'laslaslas', 'asdasa', '\n            \n            \n            \n            asdas<div>asdas</div><div>ada<b>sd</b></div><div><b>adasdas</b></div>                                        ', NULL, 'http://localhost/consejoprofesional/assetics/img/cursos/CRISTINA_FOTO_HOT5.jpg', 2),
(5, 'conferencia', '2 dias', 'salon gala', '\n            asdas<div>sdas</div><div><b>asdas</b></div>          ', 'https://www.youtube.com/watch?v=CzB5hFINC_k', 'http://localhost/consejoprofesional/assetics/img/conferencias/20151209160054.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `eventos`
--

CREATE TABLE IF NOT EXISTS `eventos` (
  `eventoid` int(11) NOT NULL AUTO_INCREMENT,
  `eventofecha` date NOT NULL,
  `inscripcion` date NOT NULL,
  `cursoid` int(11) NOT NULL,
  PRIMARY KEY (`eventoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `infoConsejo`
--

CREATE TABLE IF NOT EXISTS `infoConsejo` (
  `consejoId` int(11) NOT NULL AUTO_INCREMENT,
  `seccion` varchar(100) NOT NULL,
  `contenido` text NOT NULL,
  `html` text NOT NULL,
  `extra` text NOT NULL,
  PRIMARY KEY (`consejoId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `infoConsejo`
--

INSERT INTO `infoConsejo` (`consejoId`, `seccion`, `contenido`, `html`, `extra`) VALUES
(15, 'quienesSomos', '\n          	asdasda<div>asdasa</div><div>asdasdas</div>          ', '\n          	asdasda<div>asdasa</div><div>asdasdas</div>          ', 'http://localhost/consejoprofesional/assetics/img/20151209160054.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `inscriptos`
--

CREATE TABLE IF NOT EXISTS `inscriptos` (
  `inscriptosid` int(11) NOT NULL AUTO_INCREMENT,
  `nombreinsc` varchar(60) NOT NULL,
  `correoinsc` varchar(100) NOT NULL,
  `fechainsc` date NOT NULL,
  `telefoninsc` varchar(30) NOT NULL,
  `direccioninsc` varchar(50) NOT NULL,
  `pago` varchar(2) NOT NULL,
  PRIMARY KEY (`inscriptosid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `instituciones`
--

CREATE TABLE IF NOT EXISTS `instituciones` (
  `institutoId` int(11) NOT NULL AUTO_INCREMENT,
  `institutonombre` varchar(200) NOT NULL,
  `institutoobjetivo` text NOT NULL,
  `institutomail` varchar(100) NOT NULL,
  `institutofbFanpage` varchar(100) NOT NULL,
  `institutotwFanpage` varchar(100) NOT NULL,
  `institutologo` text NOT NULL,
  PRIMARY KEY (`institutoId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `instituciones`
--

INSERT INTO `instituciones` (`institutoId`, `institutonombre`, `institutoobjetivo`, `institutomail`, `institutofbFanpage`, `institutotwFanpage`, `institutologo`) VALUES
(4, 'Instituto de Derecho del Trabajo del Consejo de Abogados Resistencia', 'Objetivo: estudio, investigación y profundización del Derecho del Trabajo y la Seguridad Social.', '', '', '', ''),
(5, 'Instituto de Derecho de Familias y Sucesiones', 'Objetivo: trabajar de modo solidarios respecto a las particulares temáticas de las cuestiones de familia y sucesiones. Compartir experiencias promover el mejoramiento del fuero especializado, participar en distintos espacios de interés social y profesional. Con una fuerte impronta para la capacitación de los colegas y un servicio a la comunidad en estas ramas del derecho.', '', '', '', ''),
(6, 'Instituto de Derecho Penal Hans Welzel', 'Objetivo: la realización de actividades de formación académica dictando cursos sobre temas trascendentales de la praxis jurídica y que hacen a la práctica del Derecho Penal', '', '', '', ''),
(7, 'Instituto de Derecho Administrativo y Constitucional "Dr. Raúl E. Antúnez Percíncula', 'Objetivo: estudio de las relaciones de la administración pública con los administrados; las relaciones de los distintos órganos entre sí de la administración pública; a fin de satisfacer y lograr las finalidades del interés público hacia la que debe tender la Administración, como también los aspectos constitucionales del marco normativo nacional.', '', '', '', ''),
(8, 'Instituto de Derecho Privado Dra. Maria G. Lludgar', 'Integrado por la Asociación de Magistrados y Funcionarios Judiciales de la Provincia del Chaco y el Consejo de Abogados y Procuradores de la Primera Circunscripción Judicial de Resistencia  Objetivo: promover el estudio, investigación y divulgación oral y escrita de temas de derecho privado nacional,  extranjero y comunitario, organización de grupos de estudio e investigación; organizar cursos de posgrado, jornadas, congresos; facilitar el acceso de profesionales del derecho a becas de intercambio con universidades y entidades afines, privadas y públicas, efectuar publicaciones y concluir convenios de cooperación con entidades afines', '', '', '', ''),
(9, 'Instituto de Derecho Tributario', 'Objetivo: la realización de estudios e investigación del Derecho Tributario Nacional y Provincial, y Municipal, destinados al perfeccionamiento de los profesionales que integran el instituto', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `linkid` int(11) NOT NULL AUTO_INCREMENT,
  `linknombre` varchar(100) NOT NULL,
  `link` text NOT NULL,
  `linkhtml` text NOT NULL,
  `linkfecha` date NOT NULL,
  `linkhora` time NOT NULL,
  `tlid` int(11) NOT NULL,
  PRIMARY KEY (`linkid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`linkid`, `linknombre`, `link`, `linkhtml`, `linkfecha`, `linkhora`, `tlid`) VALUES
(2, 'web whatsapp', 'https://web.whatsapp.com/', '', '2015-12-27', '22:02:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `novedades`
--

CREATE TABLE IF NOT EXISTS `novedades` (
  `novedadid` int(11) NOT NULL AUTO_INCREMENT,
  `novedadfecha` date NOT NULL,
  `novedadresumen` text NOT NULL,
  `novedadcontenido` text NOT NULL,
  `novedadimg` text NOT NULL,
  `novedadhora` time NOT NULL,
  `novedadhtml` text NOT NULL,
  `homeslide` varchar(2) NOT NULL,
  `tnid` int(11) NOT NULL,
  `novedadtitulo` varchar(100) NOT NULL,
  PRIMARY KEY (`novedadid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `novedades`
--

INSERT INTO `novedades` (`novedadid`, `novedadfecha`, `novedadresumen`, `novedadcontenido`, `novedadimg`, `novedadhora`, `novedadhtml`, `homeslide`, `tnid`, `novedadtitulo`) VALUES
(1, '2015-12-27', 'papapapapapa', '\n            <font face="serif">asdasdasdasdasdas</font><div><font face="serif"><b>dasdasdasdasdasdasdasdasdas</b></font></div><div><font face="serif"><b><i>adasdasdasdasdasdas</i></b></font></div><div><font face="serif" size="5"><b><i>asadasdasdasdasdasasdasdsadas</i></b></font></div>          ', 'http://localhost/consejoprofesional/assetics/img/novedades/1005548_943752262326933_3017408031362224263_n.jpg', '12:59:02', 'papaapapap', 'si', 2, ''),
(2, '2015-12-28', 'Esta es mi prueba', '\n            <font face="serif">asdasdasdasdasdas</font><div><font face="serif"><b>dasdasdasdasdasdasdasdasdas</b></font></div><div><font face="serif"><b><i>adasdasdasdasdasdas</i></b></font></div><div><font face="serif" size="5"><b><i>asadasdasdasdasdasasdasdsadas</i></b></font></div>          ', 'http://localhost/consejoprofesional/assetics/img/novedades/slide-0.png', '00:51:13', 'esta-es-mi-prueba', 'no', 2, ''),
(3, '2015-12-28', 'Esta es mi prueba', '\n            <font face="serif">asdasdasdasdasdas</font><div><font face="serif"><b>dasdasdasdasdasdasdasdasdas</b></font></div><div><font face="serif"><b><i>adasdasdasdasdasdas</i></b></font></div><div><font face="serif" size="5"><b><i>asadasdasdasdasdasasdasdsadas</i></b></font></div>          ', 'http://localhost/consejoprofesional/assetics/img/novedades/CRISTINA_FOTO_HOT.jpg', '00:52:33', 'esta-es-mi-prueba2', 'si', 2, ''),
(4, '2015-12-27', 'asdasdas', 'asdasdasdas', 'http://localhost/consejoprofesional/assetics/img/novedades/20151209160054.jpg', '12:26:39', 'esta-es-mi-prueba4', 'no', 2, ''),
(5, '2015-12-27', 'novedad de jovenes jp', 'esta es una novedad de jp', 'http://localhost/consejoprofesional/assetics/img/jovenesprofesionales/12359997_10153829865674165_9010537109678356650_n.jpg', '17:36:44', 'esta-es-mi-prueba5', 'si', 2, ''),
(6, '2015-12-27', 'JP NOVEDAD', '\n            ADASDASDAS<div>ASDADAS</div><div>ADASDAS</div><div><b>ADASDASDASDASDAS</b></div><div>ASDASDAS</div>          ', 'http://localhost/consejoprofesional/assetics/img/jovenesprofesionales/20151209160054.jpg', '17:38:27', 'esta-es-mi-prueba6', 'no', 1, ''),
(7, '2015-12-27', 'llllllllllllllllllllllllllllllllllllllllll', '<u><font size="5">llklhkhkhkhkj</font></u><div>nkhhklhkl</div><div>kkjlkl</div><div><b>kklljljkl</b></div>', 'http://localhost/consejoprofesional/assetics/img/novedades/1005548_943752262326933_3017408031362224263_n1.jpg', '21:20:22', 'esta-es-mi-prueba7', 'no', 2, ''),
(8, '2015-12-23', 'asdsa', 'asdas', 'asdasdsa', '22:02:12', 'esta-es-mi-prueba8', 'si', 2, ''),
(9, '2015-12-23', 'asdsa', 'asdas', 'asdasdsa', '22:02:12', 'esta-es-mi-prueba9', 'si', 2, ''),
(10, '2015-12-28', 'mi novedad', '<blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;"><blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;">asdas</blockquote></blockquote>', 'http://localhost/consejoprofesional/assetics/img/novedades/12359997_10153829865674165_9010537109678356650_n1.jpg', '01:26:18', 'mi-novedad', '', 2, ''),
(11, '2015-12-28', 'asdasdasa asdas', '<blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;"><blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;">asdas</blockquote></blockquote>', 'http://localhost/consejoprofesional/assetics/img/novedades/12359997_10153829865674165_9010537109678356650_n2.jpg', '01:53:00', 'asdasdasa-asdas', '', 2, 'mi titulo'),
(12, '2015-12-28', 'asdasda ase adasda asdasdas asdasdas adasdsadas', 'asdadasda<div>asdasdas</div><div>asdasdas</div><div>asdasdas</div><div>asdasdas</div>', 'http://localhost/consejoprofesional/assetics/img/novedades/1005548_943752262326933_3017408031362224263_n.jpg', '02:23:38', 'en-el-chaco-se-lanza-ley-de-comida', '', 2, 'en el chaco se lanza ley de comida'),
(13, '2015-12-28', 'resumen de la novedad legislativa', 'sadas', 'http://localhost/consejoprofesional/assetics/img/novedadeslegislativas/12359997_10153829865674165_9010537109678356650_n.jpg', '02:25:39', 'novedad-legislativa', '', 3, 'novedad legislativa');

-- --------------------------------------------------------

--
-- Table structure for table `profesional`
--

CREATE TABLE IF NOT EXISTS `profesional` (
  `profesionalid` int(11) NOT NULL AUTO_INCREMENT,
  `profesionalnombre` varchar(60) NOT NULL,
  `profesionaltitulo` varchar(5) NOT NULL,
  `profesionaldesc` text NOT NULL,
  `cargoid` int(11) NOT NULL,
  PRIMARY KEY (`profesionalid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `profesional`
--

INSERT INTO `profesional` (`profesionalid`, `profesionalnombre`, `profesionaltitulo`, `profesionaldesc`, `cargoid`) VALUES
(1, 'pepe mujica', 'Dr', '', 2),
(2, 'pepe mujica mujer1', 'Dra', '', 7),
(3, 'pepe mujica nene', 'Dr', '', 4),
(5, 'pepe mujica mujera', 'Dra', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `sessionId` int(11) NOT NULL AUTO_INCREMENT,
  `session` varchar(100) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sessionInit` date NOT NULL,
  `sessionHour` time NOT NULL,
  `dataSession` varchar(100) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tipoCurso`
--

CREATE TABLE IF NOT EXISTS `tipoCurso` (
  `tcid` int(11) NOT NULL AUTO_INCREMENT,
  `tcnombre` varchar(100) NOT NULL,
  `tcdesc` text NOT NULL,
  PRIMARY KEY (`tcid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tipoCurso`
--

INSERT INTO `tipoCurso` (`tcid`, `tcnombre`, `tcdesc`) VALUES
(1, 'posgrados', ''),
(2, 'cursos', ''),
(3, 'conferencias', '');

-- --------------------------------------------------------

--
-- Table structure for table `tipolink`
--

CREATE TABLE IF NOT EXISTS `tipolink` (
  `tlid` int(11) NOT NULL AUTO_INCREMENT,
  `tlnombre` varchar(100) NOT NULL,
  `tldesc` text NOT NULL,
  PRIMARY KEY (`tlid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tipolink`
--

INSERT INTO `tipolink` (`tlid`, `tlnombre`, `tldesc`) VALUES
(1, 'link', ''),
(2, 'publicacion', '');

-- --------------------------------------------------------

--
-- Table structure for table `tiponovedad`
--

CREATE TABLE IF NOT EXISTS `tiponovedad` (
  `tnid` int(11) NOT NULL AUTO_INCREMENT,
  `tnnombre` varchar(100) NOT NULL,
  `tndesc` text NOT NULL,
  PRIMARY KEY (`tnid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tiponovedad`
--

INSERT INTO `tiponovedad` (`tnid`, `tnnombre`, `tndesc`) VALUES
(1, 'jovenes_profesionales', ''),
(2, 'institucionales', ''),
(3, 'legislativas', '');

-- --------------------------------------------------------

--
-- Table structure for table `tipoUsuario`
--

CREATE TABLE IF NOT EXISTS `tipoUsuario` (
  `tipoUsuarioId` int(11) NOT NULL AUTO_INCREMENT,
  `TUNombre` varchar(100) NOT NULL,
  `TUDesc` varchar(100) NOT NULL,
  PRIMARY KEY (`tipoUsuarioId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `apellido` varchar(60) NOT NULL,
  `userName` varchar(30) NOT NULL,
  `password` varchar(15) NOT NULL,
  `email` varchar(60) NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `avatar` varchar(40) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
