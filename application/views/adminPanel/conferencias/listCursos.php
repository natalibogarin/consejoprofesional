<div class="adminContainer">
  	<div class="panel panel-success" style="width:auto;">
		<div class="panel-heading">
			<h3 class="panel-title">Lista de Conferencias
				<a class="right-align" href="<?php echo base_url()?>index.php/conferenciasInfo/view/newCurso" title="Agregar nueva Instituto">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				</a>
			</h3>
		</div>
		<div class="panel-body" role="form">
			<?php if ($status=="error") :?>
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> Ha ocurrido un inconveniente al realizar la transacción
				</div>
			<?php elseif ($status=="success") :?>
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></strong> La transacción ha sido realizada con éxito
				</div>
			<?php endif?>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre de la Conferencia</th>
						<th>Duración</th>
						<th>Lugar</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($cursos as $curso) :?>
						<tr>
							<th scope="row"><?php echo $curso->cursoId ?></th>
							<td><?php echo $curso->cursoNombre ?></td>
							<td><?php echo $curso->cursoduracion ?></td>
							<td><?php echo $curso->cursodonde ?></td>
							<td>
								<a href="<?php echo base_url()?>index.php/conferenciasInfo/view/editCurso/<?php echo $curso->cursoId ?>" title="editar">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
								<a href="<?php echo base_url()?>index.php/conferenciasInfo/deleteCurso/<?php echo $curso->cursoId ?>" title="eliminar">
									<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div class="text-center">
				<ul class="pagination">
				    <?php echo $pages ?>
				</ul>
			</div>
		</div>
    </div>
</div>