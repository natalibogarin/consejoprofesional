<div class="adminContainer">
  	<div class="panel panel-success" style="width:auto;">
		<div class="panel-heading">
			<h3 class="panel-title">Lista de Autoridades</h3>
		</div>
		<div class="panel-body" role="form">
			<?php if ($status==2) :?>
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> Ha ocurrido un inconveniente al realizar la transacción
				</div>
			<?php elseif ($status==1) :?>
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></strong> La transacción ha sido realizada con éxito
				</div>
			<?php endif?>
			<form action="<?php echo base_url()?>index.php/consejoInfo/updateAutoridad" method="post" accept-charset="utf-8">
				<div class="form-group">
					<label for="profesionaltitulo">Titulo</label>
					<select name="profesionaltitulo" id="profesionaltitulo" class="form-control">
						<option value="<?php echo $autoridad->profesionaltitulo?>"><?php echo $autoridad->profesionaltitulo ?></option>
						<option value="Dr.">Dr.</option>
						<option value="Dra.">Dra.</option>
					</select>
				</div>
				<div class="form-group">
					<label for="profesionalnombre">Nombre y Apellido</label>
					<input type="text" class="form-control" id="profesionalnombre" name="profesionalnombre" value="<?php echo $autoridad->profesionalnombre?>">
					<input type="hidden" name="profesionalid" value="<?php echo $autoridad->profesionalid?>">
				</div>

				<div class="form-group">
					<label for="cargo">Cargo</label>
					<select name="cargo" id="cargo" class="form-control">
						<option value="<?php echo $autoridad->cargoid?>"><?php echo $autoridad->cargonombre?></option>
						<?php foreach ($cargos as $cargo) :?>
							<?php if ($cargo->cargoid != $autoridad->cargoid) :?>
								<option value="<?php echo $cargo->cargoid?>"><?php echo $cargo->cargonombre?></option>
							<?php endif?>
						<?php endforeach ?>
					</select>
				</div>

				<button type="submit" class="btn btn-default">Guardar</button>
			</form>
		</div>
    </div>
</div>