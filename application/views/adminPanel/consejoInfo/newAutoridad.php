<div class="adminContainer">
  	<div class="panel panel-success" style="width:auto;">
		<div class="panel-heading">
			<h3 class="panel-title">Agregar <b>Nuevo Integrante al Consejo</b></h3>
		</div>
		<div class="panel-body" role="form">

			<form action="<?php echo base_url()?>index.php/consejoInfo/newAutoridad" method="post" accept-charset="utf-8">
				<div class="form-group">
					<label for="profesionaltitulo">Título</label>
					<select name="profesionaltitulo" id="profesionaltitulo" class="form-control">
						<option value="Dr.">Dr.</option>
						<option value="Dra.">Dra.</option>
					</select>
				</div>
				<div class="form-group">
					<label for="profesionalnombre">Nombre y Apellido</label>
					<input type="text" class="form-control" id="profesionalnombre" name="profesionalnombre" placeholder="Ej, Luis Mora">
				</div>

				<div class="form-group">
					<label for="cargo">Cargo</label>
					<select name="cargo" id="cargo" class="form-control">
						<?php foreach ($cargos as $cargo) :?>
							<option value="<?php echo $cargo->cargoid?>"><?php echo $cargo->cargonombre?></option>
						<?php endforeach ?>
					</select>
				</div>

				<button type="submit" class="btn btn-default">Guardar</button>
			</form>
		</div>
    </div>
</div>