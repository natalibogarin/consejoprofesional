<script  type="text/javascript" charset="utf-8">
  var urlUpload = domain + "index.php/ConsejoInfo/upload_file";
  var urlNovedad = domain + "index.php/ConsejoInfo/saveQuienesSomos";
</script>
<div class="adminContainer">
  <div class="panel panel-success" style="width:auto;">
      <div class="panel-heading">
        <h3 class="panel-title">Agregar Contenido a: Página <b>Quienes somos</b></h3>
      </div>
      <div class="panel-body" role="form">
          <div class="alert alert-danger alert-dismissible hidden" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> Ha ocurrido un inconveniente al realizar la transacción
          </div>
          <div class="alert alert-success alert-dismissible hidden" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></strong> La transacción ha sido realizada con éxito
          </div>
        <!-- Aqui va el content de  !-->
        <div class="bs-example" style="padding-top:25px">
          <div class="form-group">
            <link href="<?php echo base_url()?>assetics/vendors/wysiwyg/external/google-code-prettify/prettify.css" rel="stylesheet">
            <script src="<?php echo base_url()?>assetics/vendors/wysiwyg/external/jquery.hotkeys.js"></script>
            <script src="<?php echo base_url()?>assetics/vendors/wysiwyg/external/google-code-prettify/prettify.js"></script>
            <link href="<?php echo base_url()?>assetics/vendors/wysiwyg/index.css" rel="stylesheet">
            <script src="<?php echo base_url()?>assetics/vendors/wysiwyg/bootstrap-wysiwyg.js"></script>
            <div id="alerts"></div>
            <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
              <div class="btn-group">
                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Fuente"><i class="glyphicon glyphicon-font"></i><b class="caret"></b></a>
                <ul class="dropdown-menu">
                </ul>
              </div>
              <div class="btn-group">
                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Tamaño de Fuente"><i class="glyphicon glyphglyphicon glyphicon-text-height"></i>&nbsp;<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a data-edit="fontSize 5"><font size="5">Grande</font></a></li>
                  <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                  <li><a data-edit="fontSize 1"><font size="1">Pequeño</font></a></li>
                </ul>
              </div>
              <div class="btn-group">
                <a class="btn" data-edit="bold" title="Negrita (Ctrl/Cmd+B)"><i class="glyphicon glyphicon-bold"></i></a>
                <a class="btn" data-edit="italic" title="Italica (Ctrl/Cmd+I)"><i class="glyphicon glyphicon-italic"></i></a>
                <a class="btn" data-edit="underline" title="Subrayado (Ctrl/Cmd+U)"><u><i>S</i></u></a>
              </div>
              <div class="btn-group">
                <a class="btn" data-edit="insertunorderedlist" title="Viñetas"><i class="glyphicon glyphicon-list"></i></a>
                <a class="btn" data-edit="outdent" title="Reducir indentación (Shift+Tab)"><i class="glyphicon glyphicon-indent-left"></i></a>
                <a class="btn" data-edit="indent" title="Indentar (Tab)"><i class="glyphicon glyphicon-indent-right"></i></a>
              </div>
              <div class="btn-group">
                <a class="btn" data-edit="justifyleft" title="Alineación Izquierda (Ctrl/Cmd+L)"><i class="glyphicon glyphicon-align-left"></i></a>
                <a class="btn" data-edit="justifycenter" title="Alineación Centro (Ctrl/Cmd+E)"><i class="glyphicon glyphicon-align-center"></i></a>
                <a class="btn" data-edit="justifyright" title="Alineación Derecha (Ctrl/Cmd+R)"><i class="glyphicon glyphicon-align-right"></i></a>
                <a class="btn" data-edit="justifyfull" title="Justificar (Ctrl/Cmd+J)"><i class="glyphicon glyphicon-align-justify"></i></a>
              </div>
              <div class="btn-group">
                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="glyphicon glyphicon-link"></i></a>
                <div class="dropdown-menu input-append">
                  <input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
                  <button class="btn" type="button">Agregar</button>
                </div>
              </div>
              <div class="btn-group">
                <a class="btn" title="Agregar imagen (or just drag & drop)" id="pictureBtn"><i class="glyphicon glyphicon-picture"></i></a>
                <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
              </div>
            </div>
          </div>
          <input type="hidden" id="seccion" name="seccion" value="quienesSomos">
          <input type="hidden" id="consejoId" name="consejoId" value="<?php echo $consejoId?>">
          <div id="editor">
          	<?php if($contenido) {echo $contenido[0]->html;}?>
          </div>
        </div>
        <div class="page-header">
        </div>
        <div class="form-group">
          <label for="novedadimg">Imagen de Referencia</label>
          <input type="file" class="form-control" id="novedadimg" name="novedadimg">
          <input type="hidden" class="form-control" id="novedadimghidden" name="novedadimghidden" value="<?php if($contenido) {echo $contenido[0]->extra;}?>">
          <div id="uploads">
            <?php if($contenido) {echo '<img src="'.$contenido[0]->extra.'" width=200>';}?>
          </div>
        </div>
        <button type="button" id="bottonSubmit" class="btn btn-lg btn-default fl-derecha">Guardar</button>
        </div>
    </div>
    <script src="<?php echo base_url()?>assetics/vendors/jquery.ajaxfileupload.js" type="text/javascript" charset="utf-8" async defer></script>
    <script src="<?php echo base_url()?>assetics/js/scriptadmin.js" type="text/javascript" charset="utf-8" async defer></script>
  </div>
</div>