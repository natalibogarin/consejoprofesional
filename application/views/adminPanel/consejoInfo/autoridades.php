<div class="adminContainer">
  	<div class="panel panel-success" style="width:auto;">
		<div class="panel-heading">
			<h3 class="panel-title">Lista de Autoridades
					<a class="btn btn-primary right-align" href="<?php echo base_url()?>index.php/consejoInfo/view/newAutoridad" title="Agregar nueva Autoridad">
					Agregar nueva Autoridad <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
					</a>
			</h3>
		</div>
		<div class="panel-body" role="form">
			<?php if ($status=="error") :?>
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> Ha ocurrido un inconveniente al realizar la transacción
				</div>
			<?php elseif ($status=="success") :?>
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></strong> La transacción ha sido realizada con éxito
				</div>
			<?php endif?>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre y Apellido</th>
						<th>Cargo</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($autoridades as $autoridad) :?>
						<tr>
							<th scope="row"><?php echo $autoridad->profesionalid ?></th>
							<td><?php echo $autoridad->cargonombre ?></td>
							<td><?php echo $autoridad->profesionaltitulo." ".$autoridad->profesionalnombre?></td>
							<td>
								<a href="<?php echo base_url()?>index.php/consejoInfo/view/editAutoridad/<?php echo $autoridad->profesionalid ?>" title="editar">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
								<a href="<?php echo base_url()?>index.php/consejoInfo/deleteAutoridad/<?php echo $autoridad->profesionalid ?>" title="eliminar">
									<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div class="text-center">
				<ul class="pagination">
				    <?php echo $pages ?>
				</ul>
			</div>
		</div>
    </div>
</div>