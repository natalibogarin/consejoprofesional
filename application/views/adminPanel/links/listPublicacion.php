<div class="adminContainer">
  	<div class="panel panel-success" style="width:auto;">
		<div class="panel-heading">
			<h3 class="panel-title">Lista de Institutos
				<a class="right-align" href="<?php echo base_url()?>index.php/linksInfo/view/newPublicacion" title="Agregar nueva Instituto">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				</a>
			</h3>
		</div>
		<div class="panel-body" role="form">
			<?php if ($status=="error") :?>
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> Ha ocurrido un inconveniente al realizar la transacción
				</div>
			<?php elseif ($status=="success") :?>
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></strong> La transacción ha sido realizada con éxito
				</div>
			<?php endif?>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre de la Publicación</th>
						<th>Fecha</th>
						<th>Hora</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($links as $link) :?>
						<tr>
							<th scope="row"><?php echo $link->linkid ?></th>
							<td><?php echo $link->linknombre ?></td>
							<td><?php echo $link->linkfecha ?></td>
							<td><?php echo $link->linkhora ?></td>
							<td>
								<a href="<?php echo base_url()?>index.php/linksInfo/view/editLink/<?php echo $link->linkid ?>" title="editar">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
								<a href="<?php echo base_url()?>index.php/linksInfo/deleteLink/<?php echo $link->linkid ?>/listPublicacion" title="eliminar">
									<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div class="text-center">
				<ul class="pagination">
				    <?php echo $pages ?>
				</ul>
			</div>
		</div>
    </div>
</div>