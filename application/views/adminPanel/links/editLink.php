<div class="adminContainer">
  <div class="panel panel-success" style="width:auto;">
      <div class="panel-heading">
        <h3 class="panel-title">Agregar <b>Nuevo Link</b></h3>
      </div>
      <div class="panel-body" role="form">
          <?php if ($status=="error") :?>
          <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> Ha ocurrido un inconveniente al realizar la transacción
          </div>
          <?php elseif ($status=="success") :?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></strong> La transacción ha sido realizada con éxito
            </div>
          <?php endif?>
          <!-- Aqui va el content de  !-->
          <form action="<?php echo base_url()?>index.php/linksInfo/editLink" method="post" accept-charset="utf-8">
            <div class="form-group">
              <label for="linknombre">Nombre del Link</label>
              <input type="text" value="<?php echo $link->linknombre ?>" class="form-control" name="linknombre" id="linknombre" placeholder="">
            </div>
            <div class="form-group">
              <label for="link">Url</label>
              <input type="text" value="<?php echo $link->link ?>" class="form-control" name="link" id="link" placeholder="">
            </div>
            <input type="hidden" value="1" class="form-control" name="tlid" id="tlid" placeholder="">
            <input type="hidden" value="<?php echo $link->linkid ?>" class="form-control" name="linkid" id="linkid" placeholder="">
            <input type="hidden" value="listLinks" class="form-control" name="where" id="where" placeholder="">
            <button type="submit" class="btn btn-lg btn-default fl-derecha">Guardar</button>
          </form>
        </div>
    </div>
  </div>
</div>