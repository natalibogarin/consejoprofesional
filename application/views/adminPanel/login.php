<!DOCTYPE html>
<html>
	<head>
		<title>Consejo Profesional de Abogados</title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetics/vendors/bootstrap-3.3.5-dist/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetics/css/adminStyle.css">
	</head>
	<body class="body-bck">
		<div class="container bodyAdmin">
			<div class="logo-admin">
				<a href="<?php echo base_url()?>index.php" target="_blank"><img src="<?php echo base_url()?>assetics/img/logo.png" alt="Consejo Profesional de Abogados y Procuradores de Resistencia"></a>
			</div>
			<div class="name">Consejo Profesional de Abogados y Procuradores de Resistencia</div>
	        <div class="row">
	            <div class="col-md-4 col-md-offset-4">
	                <div class="login-panel panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">Panel de Administraci&oacute;n</h3>
	                    </div>
	                    <div class="panel-body">
	                    <form action="<?php echo base_url()?>index.php/adminPanel/loggin" method="post">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Usuario" name="username" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Contraseña" name="pass" type="password">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-lg btn-success btn-block">Ingresar</button>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
		<script src="<?php echo base_url()?>assetics/js/jquery-2.1.4.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo base_url()?>assetics/vendors/bootstrap-3.3.5-dist/js/bootstrap.min.js" type="text/javascript" charset="utf-8" ></script>
	</body>
</html>