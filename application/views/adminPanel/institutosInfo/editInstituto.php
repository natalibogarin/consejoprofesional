<div class="adminContainer">
  	<div class="panel panel-success" style="width:auto;">
		<div class="panel-heading">
			<h3 class="panel-title">Editar Institución </h3>
		</div>
		<div class="panel-body" role="form">
			<?php if ($status=="error") :?>
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> Ha ocurrido un inconveniente al realizar la transacción
				</div>
			<?php elseif ($status=="success") :?>
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></strong> La transacción ha sido realizada con éxito
				</div>
			<?php endif?>
			<form action="<?php echo base_url()?>index.php/institucionesInfo/updateInstituto" method="post" accept-charset="utf-8">
				<div class="form-group">
					<label for="institutonombre">Nombre del Instituto</label>
					<input type="text" class="form-control" value="<?php echo $instituto->institutonombre?>" id="institutonombre" name="institutonombre">
					<input type="hidden" value="<?php echo $instituto->institutoId?>" id="institutoId" name="institutoId">
				</div>
				<div class="form-group">
					<label for="institutoobjetivo">Objetivo del Instituto</label>
					<input type="text" class="form-control" value="<?php echo $instituto->institutoobjetivo?>" id="institutoobjetivo" name="institutoobjetivo" >
				</div>
				<div class="form-group">
					<label for="institutomail">Correo Electrónico</label>
					<input type="text" class="form-control" value="<?php echo $instituto->institutomail?>" id="institutomail" name="institutomail">
				</div>
				<div class="form-group">
					<label for="institutofbFanpage">Facebook Fan Page</label>
					<input type="text" class="form-control" value="<?php echo $instituto->institutofbFanpage?>" id="institutofbFanpage" name="institutofbFanpage">
				</div>
				<div class="form-group">
					<label for="institutotwFanpage">Twiter Fan Page</label>
					<input type="text" class="form-control" value="<?php echo $instituto->institutotwFanpage?>" id="institutotwFanpage" name="institutotwFanpage">
				</div>
				<div class="form-group">
					<label for="institutologo">Logo</label>
					<input type="text" class="form-control" value="<?php echo $instituto->institutologo?>" id="institutologo" name="institutologo">
				</div>
				<button type="submit" class="btn btn-default">Guardar</button>
			</form>
		</div>
    </div>
</div>


