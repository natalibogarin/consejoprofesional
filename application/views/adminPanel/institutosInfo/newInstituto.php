<div class="adminContainer">
  	<div class="panel panel-success" style="width:auto;">
		<div class="panel-heading">
			<h3 class="panel-title">Agregar <b>Nueva Institución</b></h3>
		</div>
		<div class="panel-body" role="form">
			<?php if ($status=="error") :?>
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> Ha ocurrido un inconveniente al realizar la transacción
				</div>
			<?php elseif ($status=="success") :?>
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></strong> La transacción ha sido realizada con éxito
				</div>
			<?php endif?>
			<form action="<?php echo base_url()?>index.php/institucionesInfo/newInstituto" method="post" accept-charset="utf-8">
				<div class="form-group">
					<label for="institutonombre">Nombre del Instituto</label>
					<input type="text" class="form-control" id="institutonombre" name="institutonombre" placeholder="Ej, Instituto De Abogados">
				</div>
				<div class="form-group">
					<label for="institutoobjetivo">Objetivo del Instituto</label>
					<input type="text" class="form-control" id="institutoobjetivo" name="institutoobjetivo" placeholder="Ej, El objetivo es...">
				</div>
				<div class="form-group">
					<label for="institutomail">Correo Electrónico</label>
					<input type="text" class="form-control" id="institutomail" name="institutomail" placeholder="Ej, ejemplo@instituto.com">
				</div>
				<div class="form-group">
					<label for="institutofbFanpage">Facebook Fan Page</label>
					<input type="text" class="form-control" id="institutofbFanpage" name="institutofbFanpage">
				</div>
				<div class="form-group">
					<label for="institutotwFanpage">Twiter Fan Page</label>
					<input type="text" class="form-control" id="institutotwFanpage" name="institutotwFanpage">
				</div>
				<div class="form-group">
					<label for="institutologo">Logo</label>
					<input type="text" class="form-control" id="institutologo" name="institutologo">
				</div>
				<button type="submit" class="btn btn-default">Guardar</button>
			</form>
		</div>
    </div>
</div>


