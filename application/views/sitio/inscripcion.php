 <!-- content -->
<section class="container-fluid">
<div class="row">
    <div class="content col-xs-12 col-md-9 col-lg-9">
        <div class="row form-inscripcion">
            <div class="header-consejo">
                <svg viewBox="0 0 32 32" class="art-icon-header">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo base_url()?>assetics/img/icons/cursos-icon.svg#cursos"></use>
                </svg>
                <h4 class="header-title">Formulario de inscripción</h4>
            </div>
            <?php if ($param1=="error") :?>
              <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> Ha ocurrido un inconveniente al realizar la transacción
              </div>
            <?php elseif ($param1=="success") :?>
              <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></strong> La transacción ha sido realizada con éxito
              </div>
            <?php endif?>
            <div class="col-xs-1 col-md-1 col-lg-1"></div>
            <div class="col-xs-12 col-md-10 col-lg-10">
               <form class="form-horizontal" action="<?php echo base_url()?>index.php/sitio/sendEmail" method="post" role="form">
                <fieldset>
                    <legend>
                        <h4>Inscripción a curso "Nombre de curso"</h4>
                        <p class="t-sub2">Complete los campos que se solicitan, los datos declarados en este Formulario
                        tienen carácter de DECLARACIÓN JURADA.</p>
                    </legend>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <div class="form-group">
                                <label class="lbl control-label col-lg-4">Nombre y Apellido</label>
                                <div class="controls col-lg-8">
                                    <input type="text" class="form-control" name="nombre">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lbl control-label col-lg-4">Teléfono de contacto</label>
                                <div class="controls col-lg-8">
                                    <input type="text" class="form-control"name="tel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lbl control-label col-lg-4">Dirección de Correo Electrónico</label>
                                <div class="controls col-lg-8">
                                    <input type="text" class="form-control" name="mail">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lbl control-label col-lg-4">Domicilio</label>
                                <div class="controls col-lg-8">
                                    <input type="text" class="form-control" name="direccion">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right btn-align">Inscribirme</button>
                    </div>
                            </fieldset>
                        </form>
            </div> 
                    
          </div>          

        <div class="row-fluid">
            <article class="logo-footer col-sm-3 col-lg-3"> </article>
            <div class="row">
                <p class="title-prefoot">CONSEJO PROFESIONAL DE ABOGADOS Y PROCURADORES DE RESISTENCIA</p>
                <article class="pre-footer col-sm-3 col-lg-3"> 
                    <ul>
                        <li><a href="" title="">El Consejo</a></li>
                        <li><a href="" title="">Novedades</a></li>
                        <li><a href="" title="">Cursos</a></li>
                        <li><a href="" title="">Novedades Legislativas</a></li>
                        <li><a href="" title="">Institutos</a></li>
                    </ul>
                </article>
                <article class=" pre-footer col-sm-2 col-lg-2"> 
                    <div class="follow">Seguinos en:</div>
                    <div class="row-fluid">
                    <div class="col-sm-6 col-lg-6 align-fb"><a href="" title=""><div class="logo-fb"></div></a></div>
                    <div class="col-sm-6 col-lg-6 align-tw"><a href="" title=""><div class="logo-tw"></div></a></div>
                    </div>
                </article>
                <article class=" pre-footer col-sm-3 col-lg-3"> 
                    Auspician:

                </article>
            </div>
        </div>
    </div>

