<!-- content -->
<section class="container-fluid">
<div class="row">
<div class="content col-xs-12 col-md-9 col-lg-9 " >
    <!-- Slider -->
    <div class="container-fluid">
        <!-- Slider -->
        <div class="row-fluid slider">
            <div class="col-xs-12 col-sm-12 col-lg-12" id="slider">
                <!-- Top part of the slider -->
                <div class="row">
                    <div class="col-xs-12 col-sm-9 col-lg-7" id="carousel-bounding-box" style="box-shadow: 15px 2px 4px #ccc;">
                        <div class="carousel slide" id="myCarousel">
                            <!-- Carousel items -->
                            <div class="carousel-inner">
                                <?php foreach ($slider as $key => $slide) :?>
                                    <div class="item <?php if ($key == 0): ?>active<?php endif ?>" data-slide-number="<?php echo $slide->novedadid?>">
                                        <img width="485" height="20" src="<?php echo $slide->novedadimg?>" class="img-size">
                                    </div>
                                <?php endforeach ?>
                            </div><!-- Carousel nav -->
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>                                       
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>                                       
                            </a>                                
                        </div>
                    </div>
                    <div class="slide-text col-md-3 col-lg-5" id="carousel-text"></div>
                    <div id="slide-content" style="display: none;">
                        <?php foreach ($slider as $slide) :?>
                            <div id="slide-content-<?php echo $slide->novedadid?>">
                                <h2 class="title-slide"><?php echo $slide->novedadtitulo?></h2>
                                <p class="sub-text"><?php echo $slide->novedadfecha." - ".$slide->novedadhora?></p>
                                <p class="text-slide"> <?php echo $slide->novedadresumen?> </p>
                                <div class="row">
                                 <div class="col-xs-12 col-md-12 col-lg-12">
                                    <a class="btn btn-news" href="<?php echo base_url()?>index.php/sitio/page/detalle-novedades/<?php echo $slide->novedadhtml?>">Leer más</a>
                                  </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/Slider-->
    <!--Blog articles-->
    <div class="blog-content">
        <div id="categories-list" class="row list-group col-xs-12 col-md-12 col-lg-12">
            <?php foreach ($novedades as $key => $novedad) :?>
            <article class="item  col-xs-12 col-md-4 col-lg-4">
                <div class="thumbnail">
                    <div class="art-header">
                        <svg viewBox="0 0 32 32" class="art-icon">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo base_url()?>assetics/img/icons/novedades-icon.svg#novedades"></use>
                         </svg>
                        <h4 class="group inner list-group-item-heading art-title"><?php echo $novedad->novedadtitulo?></h4>
                    </div>
                    <div class="caption">
                        <p class="group inner list-group-item-text art-detail">
                            <?php echo $novedad->novedadresumen?>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-lg-12">
                                <a class="btn btn-news" href="<?php echo base_url()?>index.php/sitio/page/detalle-novedades/<?php echo $novedad->novedadhtml?>">Leer más</a>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <?php endforeach ?>
        </div>
    </div>
<!--/Blog articles-->
<div class="row-fluid">
    <article class="logo-footer col-sm-3 col-lg-3"> </article>
    <div class="row">
        <p class="title-prefoot">CONSEJO PROFESIONAL DE ABOGADOS Y PROCURADORES DE RESISTENCIA</p>
        <article class="pre-footer col-sm-3 col-lg-3"> 
            <ul>
                <li><a href="" title="">El Consejo</a></li>
                <li><a href="" title="">Novedades</a></li>
                <li><a href="" title="">Cursos</a></li>
                <li><a href="" title="">Novedades Legislativas</a></li>
                <li><a href="" title="">Institutos</a></li>
            </ul>
        </article>
        <article class=" pre-footer col-sm-2 col-lg-2"> 
            <div class="follow">Seguinos en:</div>
           <div class="table-responsive follow">
            <table>
                <tbody>
                    <tr>
                        <td><a href="" title=""><img src="<?php echo base_url()?>assetics/img/logo-fb.png" alt=""></a></td>
                        <td><a href="" title=""><img src="<?php echo base_url()?>assetics/img/logo-tw.png" alt=""></a></td>
                    </tr>
                </tbody>
            </table>  
            </div>
        </article>
        <article class=" pre-footer col-sm-3 col-lg-3"> 
            Auspician:
        </article>
        </div>
    </div>
</div>
