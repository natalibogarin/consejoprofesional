 <!-- content -->
<section class="container-fluid">
<div class="row">
<div class="content col-xs-12 col-md-9 col-lg-9">
    <div class="row">
        <div class="header-news">
            <h4 class="header-title"><?php echo $novedad->novedadtitulo?></h4>
        </div>
        <article class="detail-news">
            <p><span class="glyphicon glyphicon-time"></span> <?php echo $novedad->novedadfecha ?></p>

                <hr>
                <p class="lead">
                    <?php echo $novedad->novedadresumen ?>
                </p>
                <!-- Preview Image -->
                <img class="img-responsive" src="<?php echo $novedad->novedadimg ?>" alt="">

                <hr>

                <!-- Post Content -->
                <p>
                    <?php echo $novedad->novedadcontenido ?>
                </p>
                <hr>
        </article>
    </div>

    <!--Pre-footer-->
    <div class="row-fluid">
        <article class="logo-footer col-sm-3 col-lg-3"> </article>
            <div class="row">
                <p class="title-prefoot">CONSEJO PROFESIONAL DE ABOGADOS Y PROCURADORES DE RESISTENCIA</p>
                <article class="pre-footer col-sm-3 col-lg-3"> 
                 <ul>
                    <li><a href="" title="">El Consejo</a></li>
                    <li><a href="" title="">Novedades</a></li>
                    <li><a href="" title="">Cursos</a></li>
                    <li><a href="" title="">Novedades Legislativas</a></li>
                    <li><a href="" title="">Institutos</a></li>
                </ul>
                </article>
                <article class=" pre-footer col-sm-2 col-lg-2"> 
                    Seguinos en:
                    <a href="" title=""><div class="logo-fb"></div></a>
                    <a href="" title=""><div class="logo-tw"></div></a>
                </article>
                <article class=" pre-footer col-sm-3 col-lg-3"> 
                    Auspician:

                </article>
            </div>
        </div>
      <!--/Pre-footer-->
</div>


