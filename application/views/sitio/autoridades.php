 <!-- content -->
<section class="container-fluid">
<div class="row">
<div class="content col-xs-12 col-md-9 col-lg-9">
    <div class="row">
        <div class="header-consejo">
            <object class="header-icon" type="image/svg+xml" data="<?php echo base_url()?>assetics/img/icons/white/consejo-icon.svg"></object>
            <h4 class="header-title">Consejo Profesional de Abogados y Procuradores de Resistencia</h4>
        </div>
        <h3 class="header-autoridades">Autoridades</h3>
        <!--Presidente y Vice-Presidente-->
        <div class="row">
            <article class="col-xs-12 col-sm-2 col-lg-2"></article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <img class="autoridades" src="<?php echo base_url()?>assetics/img/img_presidente.png" alt="">
                <div class="table-responsive">
                    <table class="table">
                        <tr class="table-autoridades">
                             <td class="table-cargo">PRESIDENTE</td>
                             <td>Dr. Hilario José BISTOLETTI</td>
                        </tr>
                    </table>
                </div>
            </article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <img class="autoridades" src="<?php echo base_url()?>assetics/img/img_presidente.png" alt="">
                <div class="table-responsive">
                    <table class="table">
                        <tr class="table-autoridades">
                             <td class="table-cargo">VICEPRESIDENTE</td>
                             <td>Dr. Oscar Alejandro CLEMENTE GUTIERREZ</td>
                        </tr>
                    </table>
                </div>
            </article>
        </div>
        <!--Todos los demás cargos-->
        <div class="row">
            <article class="col-xs-12 col-sm-2 col-lg-2"></article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <tr class="table-autoridades">
                             <td class="table-cargo">SECRETARIO</td>
                             <td>Dr. José Miguel VIGIER</td>
                        </tr>
                    </table>
                </div>
            </article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <tr class="table-autoridades">
                             <td class="table-cargo">PRO SECRETARIO</td>
                             <td>Dra. Marcela GONZALEZ</td>
                        </tr>
                    </table>
                </div>
            </article>
        </div>
        <div class="row">
            <article class="col-xs-12 col-sm-2 col-lg-2"></article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <tr class="table-autoridades">
                             <td class="table-cargo">TESORERO</td>
                             <td>Dr. Ricardo José URTURI</td>
                        </tr>
                    </table>
                </div>
            </article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <tr class="table-autoridades">
                             <td class="table-cargo">PRO TESORERO</td>
                             <td>Dra. Elisa E. FERNANEZ ASELLE</td>
                        </tr>
                    </table>
                </div>
            </article>
        </div>
        <div class="row">
            <article class="col-xs-12 col-sm-2 col-lg-2"></article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="table-cargo">
                            <tr>
                                <td>VOCALES TITULARES</td>
                            </tr>
                        </thead>
                        <tr>
                            <td class="table-autoridades">
                                <div class="table-vocales"> Dr. Roberto Eduardo MENA</div>
                                <div class="table-vocales"> Dra. Mariana  Yamile LUQUE ROLON</div>
                                <div class="table-vocales"> Dr. Avelino Mario Daniel FERNANDEZ</div>
                                <div class="table-vocales"> Dr. Miguel Alejandro ARMOA</div>
                                <div class="table-vocales"> Dr. Javier Hector MARTINEZ</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="table-cargo">
                            <tr>
                                <td>VOCALES SUPLENTES</td>
                            </tr>
                        </thead>
                        <tr>
                            <td class="table-autoridades">
                                <div class="table-vocales"> Dra. Aida Elisa BRISIGHELLI</div>
                                <div class="table-vocales"> Dr. Oscar Manuel CARBALLO</div>
                                <div class="table-vocales"> Dr. Juan Carlos SEGOVIA</div>
                                <div class="table-vocales"> Dra. Miriam ASTUDILLO</div>
                                <div class="table-vocales"> Dra. Marisel VILMA ROJAS</div>
                                <div class="table-vocales"> Dra. Miriam RAMIREZ</div>
                                <div class="table-vocales"> Dra. Miriam Beatriz SERIAL</div>
                                <div class="table-vocales"> Dra. Yamila MENDEZ</div>
                                <div class="table-vocales"> Dra. Hortencia AYALA MARINICH</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </article>
        </div>
       <div class="row">
            <article class="col-xs-12 col-sm-2 col-lg-2"></article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="table-cargo">
                            <tr>
                                <td>REVISORES DE CUENTA TITULARES</td>
                            </tr>
                        </thead>
                        <tr>
                            <td class="table-autoridades">
                                <div class="table-vocales"> Dra. Susana Marta WOLCOFF</div>
                                <div class="table-vocales"> Dr. Benjamín Edgardo KAPEICA</div>
                                <div class="table-vocales"> Dr. Jose Alvis WETTSTEIN</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="table-cargo">
                            <tr>
                                <td>REVISORES DE CUENTA SUPLENTES</td>
                            </tr>
                        </thead>
                        <tr>
                            <td class="table-autoridades">
                                <div class="table-vocales"> Dra. Olga Beatriz SALOM</div>
                                <div class="table-vocales"> Dr. Edgardo MORBIDONI</div>
                                <div class="table-vocales"> Dr. Diego TOLOSA</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </article>
        </div>
        <div class="row">
            <article class="col-xs-12 col-sm-2 col-lg-2"></article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="table-cargo">
                            <tr>
                                <td>REPRESENTANTES TITULARES ANTE LA FACA</td>
                            </tr>
                        </thead>
                        <tr>
                            <td class="table-autoridades">
                                <div class="table-vocales"> Dr. José Alejandro SANCHEZ</div>
                                <div class="table-vocales"> Dr. Cesar Ricardo ALEGRE</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="table-cargo">
                            <tr>
                                <td>REPRESENTANTES SUPLENTES ANTE LA FACA</td>
                            </tr>
                        </thead>
                        <tr>
                            <td class="table-autoridades">
                                <div class="table-vocales"> Dr. Gustavo Javier LOPEZ</div>
                                <div class="table-vocales"> Dra. Fabiana OJEDA</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </article>
        </div>
        <div class="row">
            <article class="col-xs-12 col-sm-2 col-lg-2"></article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="table-cargo">
                            <tr>
                                <td>INTEGRANTES TITULARES TRIBUNAL DE ETICA PROFESIONAL</td>
                            </tr>
                        </thead>
                        <tr>
                            <td class="table-autoridades">
                                <div class="table-vocales"> Dr. Oscar Arnaldo CLEMENTE</div>
                                <div class="table-vocales"> Dra. Rosa Hortencia MARINICH</div>
                                <div class="table-vocales"> Dr. Miguel Angel MORESCHI</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </article>
            <article class="col-xs-12 col-sm-4 col-lg-4">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="table-cargo">
                            <tr>
                                <td>INTEGRANTES SUPLENTES TRIBUNAL DE ETICA PROFESIONAL</td>
                            </tr>
                        </thead>
                        <tr>
                            <td class="table-autoridades">
                                <div class="table-vocales"> Dra. Sonia Cristina SEBA</div>
                                <div class="table-vocales"> Dr. Francisco ORTIZ</div>
                                <div class="table-vocales"> Dr. Victor Rolando ARBUES</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </article>
        </div>

<br><br><br>
    <div class="row-fluid">
    <article class="logo-footer col-sm-3 col-lg-3"> </article>
    <div class="row">
        <p class="title-prefoot">CONSEJO PROFESIONAL DE ABOGADOS Y PROCURADORES DE RESISTENCIA</p>
        <article class="pre-footer col-sm-3 col-lg-3"> 
            <ul>
                <li><a href="" title="">El Consejo</a></li>
                <li><a href="" title="">Novedades</a></li>
                <li><a href="" title="">Cursos</a></li>
                <li><a href="" title="">Novedades Legislativas</a></li>
                <li><a href="" title="">Institutos</a></li>
            </ul>
        </article>
        <article class=" pre-footer col-sm-2 col-lg-2"> 
            <div class="follow">Seguinos en:</div>
           <div class="table-responsive follow">
            <table>
                <tbody>
                    <tr>
                        <td><a href="" title=""><img src="<?php echo base_url()?>assetics/img/logo-fb.png" alt=""></a></td>
                        <td><a href="" title=""><img src="<?php echo base_url()?>assetics/img/logo-tw.png" alt=""></a></td>
                    </tr>
                </tbody>
            </table>  
            </div>
        </article>
        <article class=" pre-footer col-sm-3 col-lg-3"> 
            Auspician:
        </article>
        </div>
    </div>
</div>

</div>

