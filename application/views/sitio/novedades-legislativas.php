 <!-- content -->
<section class="container-fluid">
<div class="row">
<div class="content col-xs-12 col-md-9 col-lg-9">
    <div class="row">
        <div class="header-news">
            <object class="header-icon" type="image/svg+xml" data="<?php echo base_url()?>assetics/img/icons/white/newslegislativas-icon.svg"></object>
            <h4 class="header-title">Novedades Legislativas</h4>
        </div>
            <!--Blog articles-->
    <div class="blog-content">
    <div id="categories-list" class="row list-group col-xs-12 col-md-12 col-lg-12">
        <?php foreach ($novedades as $key => $novedad) :?>
            <article class="item  col-xs-12 col-md-4 col-lg-4">
                <div class="thumbnail">
                    <div class="art-header">
                        <object class="art-icon" type="image/svg+xml" data="<?php echo base_url()?>assetics/img/icons/white/newslegislativas-icon.svg"></object>
                        <h4 class="group inner list-group-item-heading art-title"><?php echo $novedad->novedadtitulo?></h4>
                    </div>
                    <div class="caption">
                        <p class="group inner list-group-item-text art-detail">
                            <?php echo $novedad->novedadresumen?>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-lg-12">
                                <a class="btn btn-news" href="<?php echo base_url()?>index.php/sitio/page/detalle-novedades/<?php echo $novedad->novedadhtml?>">Leer más</a>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        <?php endforeach ?>
    </div>
    </div>
<!--/Blog articles-->
</div>
<div class="row">
    <div class="col-sm-4 col-lg-4"></div>
    <div class="col-sm-4 col-lg-4">
        <nav>
        <ul class="pagination">
                <?php echo $pages ?>
            </ul>
        </nav>
    </div>
    <div class="col-sm-4 col-lg-4"></div>
</div>
    <div class="row-fluid">
    <article class="logo-footer col-sm-3 col-lg-2"> </article>
    <div class="row">
        <p class="title-prefoot">CONSEJO PROFESIONAL DE ABOGADOS Y PROCURADORES DE RESISTENCIA</p>
        <article class="pre-footer col-sm-3 col-lg-3"> 
            <ul>
                <li><a href="" title="">El Consejo</a></li>
                <li><a href="" title="">Novedades</a></li>
                <li><a href="" title="">Cursos</a></li>
                <li><a href="" title="">Novedades Legislativas</a></li>
                <li><a href="" title="">Institutos</a></li>
            </ul>
        </article>
        <article class=" pre-footer col-sm-2 col-lg-1"> 
            Seguinos en:
            <a href="" title=""><div class="logo-fb"></div></a>
            <a href="" title=""><div class="logo-tw"></div></a>
        </article>
        <article class=" pre-footer col-sm-3 col-lg-3"> 
            Auspician:

        </article>
    </div>
</div>

</div>

