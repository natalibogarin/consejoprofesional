 <!-- content -->
<section class="container-fluid">
<div class="row">
<div class="content col-xs-12 col-md-9 col-lg-9">
    <div class="row">
        <div class="header-news">
            <h4 class="header-title">Post-grados y especializaciones de larga duración</h4>
        </div>
            <!--Blog articles-->
        <div class="blog-content">
            <div id="categories-list" class="row list-group col-xs-12 col-md-12 col-lg-12">
                <?php //foreach ($cursos as $key => $curso) :?>
                    <article class="item  col-xs-12 col-md-4 col-lg-4">
                        <div class="thumbnail">
                            <div class="art-header">
                                <svg viewBox="0 0 32 32" class="art-icon">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo base_url()?>assetics/img/icons/cursos-icon.svg#cursos"></use>
                                </svg>
                                <h4 class="group inner list-group-item-heading art-title">Curso sobre Indivision Hereditaria<?php //echo $curso->cursotitulo?></h4>
                            </div>
                            <div class="caption">
                                <p class="group inner list-group-item-text art-detail">
                                    <?php //echo $curso->novedadresumen?>
                                    Lorem ipsum lorem ipsum ipsum lorem ipsum ipsum lorem ipsum ipsum lorem ipsum 
                                    ipsum lorem ipsum ipsum lorem ipsum ipsum lorem ipsum ipsum lorem ipsum ipsum lorem 
                                    ipsum ipsum lorem ipsum ipsum lorem ipsum ipsum lorem ipsum ipsum lorem ipsum ipsum lorem ipsum ...
                                </p>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <a class="btn btn-news" href="<?php echo base_url()?>index.php/sitio/page/detalle-curso/<?php //echo $novedad->novedadhtml?>">Más información</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="item  col-xs-12 col-md-4 col-lg-4">
                        <div class="thumbnail">
                            <div class="art-header">
                                <svg viewBox="0 0 32 32" class="art-icon">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo base_url()?>assetics/img/icons/cursos-icon.svg#cursos"></use>
                                </svg>
                                <h4 class="group inner list-group-item-heading art-title">Curso sobre Indivision Hereditaria<?php //echo $curso->cursotitulo?></h4>
                            </div>
                            <div class="caption">
                                <img src="<?php echo base_url()?>assetics/img/cursos_example.png" class="img-detail" align="center" alt="">
                                <p class="group inner list-group-item-text art-detail">
                                    <?php //echo $curso->novedadresumen?>
                                    Lorem ipsum lorem ipsum ipsum lorem   ...
                                </p>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <a class="btn btn-news" href="<?php echo base_url()?>index.php/sitio/page/detalle-curso/<?php //echo $novedad->novedadhtml?>">Más información</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                <?php //endforeach ?>
            </div>
        </div>
    <!--/Blog articles-->
    </div>
    <div class="row">
        <div class="col-sm-4 col-lg-4"></div>
        <div class="col-sm-4 col-lg-4">
            <nav>
            <ul class="pagination">
                <?php //echo $pages ?>
            </ul>
            </nav>
        </div>
        <div class="col-sm-4 col-lg-4"></div>
    </div>
    <div class="row-fluid">
        <article class="logo-footer col-sm-3 col-lg-2"> </article>
        <div class="row">
            <p class="title-prefoot">CONSEJO PROFESIONAL DE ABOGADOS Y PROCURADORES DE RESISTENCIA</p>
            <article class="pre-footer col-sm-3 col-lg-3"> 
                <ul>
                    <li><a href="" title="">El Consejo</a></li>
                    <li><a href="" title="">Novedades</a></li>
                    <li><a href="" title="">Cursos</a></li>
                    <li><a href="" title="">Novedades Legislativas</a></li>
                    <li><a href="" title="">Institutos</a></li>
                </ul>
            </article>
            <article class=" pre-footer col-sm-2 col-lg-1"> 
                Seguinos en:
                <a href="" title=""><div class="logo-fb"></div></a>
                <a href="" title=""><div class="logo-tw"></div></a>
            </article>
            <article class=" pre-footer col-sm-3 col-lg-3"> 
                Auspician:

            </article>
        </div>
    </div>
</div>

