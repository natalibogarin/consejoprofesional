 <!-- content -->
<section class="container-fluid">
<div class="row">
    <div class="content col-xs-12 col-md-9 col-lg-9">
        <div class="row">
            <div class="header-consejo">
                <object class="header-icon" type="image/svg+xml" data="<?php echo base_url()?>assetics/img/icons/white/institutos-icon.svg"></object>
                <h4 class="header-title">Institutos</h4>
            </div>
            <?php foreach ($institutos as $key => $instituto) :?>
            <div class="row">
                <div class="col-xs-1 col-md-1 col-lg-1"></div>
                <div class="col-xs-12 col-md-9 col-lg-9 instituto">
                    <div class="header-institutos">
                        <div class="detail-institutos"><object class="icon-institutos" type="image/svg+xml" data="<?php echo base_url()?>assetics/img/icons/white/institutos-icon.svg"></object></div>
                        <div class="title-instituto"><?php echo $instituto->institutonombre?></div>
                    </div>
                    <article class="detail-consejo">
                        <p><?php echo $instituto->institutoobjetivo?></p>
                        <div class="row-fluid"> 
                            <div class="col-xs-1 col-sm-1 col-lg-1"></div>
                            <div class="col-xs-12 col-sm-6 col-lg-4" style="background:#ccc;">
                                Facebook: <?php echo $instituto->institutofbFanpage?>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-lg-4" style="background:#ddd;">
                                Mail: <?php echo $instituto->institutomail?>
                            </div>
                             <div class="col-xs-2 col-sm-2 col-lg-2"></div>
                        </div>
                    </article>
                </div>
                <br>
            </div>
        <?php endforeach ?>

        <div class="row-fluid">
            <article class="logo-footer col-sm-3 col-lg-3"> </article>
            <div class="row">
                <p class="title-prefoot">CONSEJO PROFESIONAL DE ABOGADOS Y PROCURADORES DE RESISTENCIA</p>
                <article class="pre-footer col-sm-3 col-lg-3"> 
                    <ul>
                        <li><a href="" title="">El Consejo</a></li>
                        <li><a href="" title="">Novedades</a></li>
                        <li><a href="" title="">Cursos</a></li>
                        <li><a href="" title="">Novedades Legislativas</a></li>
                        <li><a href="" title="">Institutos</a></li>
                    </ul>
                </article>
                <article class=" pre-footer col-sm-2 col-lg-2"> 
                    <div class="follow">Seguinos en:</div>
                    <div class="row-fluid">
                    <div class="col-sm-6 col-lg-6 align-fb"><a href="" title=""><div class="logo-fb"></div></a></div>
                    <div class="col-sm-6 col-lg-6 align-tw"><a href="" title=""><div class="logo-tw"></div></a></div>
                    </div>
                </article>
                <article class=" pre-footer col-sm-3 col-lg-3"> 
                    Auspician:

                </article>
            </div>
        </div>
    </div>
    </div>

