<!-- nav -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Toggle mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      	<li>
          <a href="<?php echo base_url()?>index.php" class="icon-nav">
              <svg viewBox="0 0 32 32" class="nav-img">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo base_url()?>assetics/img/icons/home-icon.svg#home"></use>
              </svg>
              <div class="nav-link">
                Inicio
              </div>
           </a>
        </li>
      	<li class="dropdown">
          <a href="#" class="dropdown-toggle icon-nav" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <svg viewBox="0 0 32 32" class="nav-img">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo base_url()?>assetics/img/icons/consejo-icon.svg#consejo"></use>
              </svg>
              <div class="nav-link">
                El Consejo
                <span class="caret"></span>
              </div>
          </a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo base_url()?>index.php/sitio/page/quienesomos">Quienes Somos</a></li>
            <li><a href="<?php echo base_url()?>index.php/sitio/page/autoridades">Autoridades</a></li>
          </ul>
        </li>
        <li><a href="<?php echo base_url()?>index.php/sitio/page/novedades" class="icon-nav">
          <svg viewBox="0 0 32 32" class="nav-img">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo base_url()?>assetics/img/icons/novedades-icon.svg#novedades"></use>
          </svg>
          <div class="nav-link">
            Novedades
          </div>
          </a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle icon-nav" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <svg viewBox="0 0 32 32" class="nav-img">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo base_url()?>assetics/img/icons/cursos-icon.svg#cursos"></use>
              </svg>
              <div class="nav-link">
                Cursos
                <span class="caret"></span>
              </div>
          <ul class="dropdown-menu">
            <li><a href="<?php echo base_url()?>index.php/sitio/page/cursos-seminarios">Cursos</a></li>
            <li><a href="<?php echo base_url()?>index.php/sitio/page/conferencias">Conferencias</a></li>
            <li><a href="<?php echo base_url()?>index.php/sitio/page/posgrados">Posgrados</a></li>
          </ul>
        </li>
        <li><a href="<?php echo base_url()?>index.php/sitio/page/novedades-legislativas" class="icon-nav">
          <svg viewBox="0 0 32 32" class="nav-img">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo base_url()?>assetics/img/icons/legislativas-icon.svg#legislativas"></use>
          </svg>
          <div class="nav-link0">
            Novedades Legislativas
          </div>
        </a>
        </li>
        <li><a href="<?php echo base_url()?>index.php/sitio/page/institutos" class="icon-nav">
        <svg viewBox="0 0 32 32" class="nav-img">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo base_url()?>assetics/img/icons/institutos-icon.svg#institutos"></use>
          </svg>
          <div class="nav-link">Institutos</div>
        </a></li>
        <li><a href="<?php echo base_url()?>index.php/sitio/page/contacto" class="icon-nav">
        <i class="fa fa-envelope-square fa-2x"></i>
        <div class="nav-link">Contacto</div></a></li>
      </ul>
      <form class="navbar-form search navbar-right" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Buscar" name="q">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
	  </form>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- /nav -->