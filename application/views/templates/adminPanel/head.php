<!DOCTYPE html>
<html>
<head>
	<title>Panel de Administración - Consejo Profesional de Abogados </title>
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>assetics/img/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>assetics/img/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>assetics/img/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>assetics/img/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>assetics/img/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>assetics/img/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>assetics/img/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>assetics/img/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>assetics/img/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url();?>assetics/img/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>assetics/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assetics/img/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assetics/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo base_url();?>assetics/img/favicon/manifest.json">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetics/vendors/bootstrap-3.3.5-dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetics/css/adminStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetics/vendors/dist/css/sb-admin-2.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetics/vendors/dist/css/timeline.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetics/vendors/metisMenu/dist/metisMenu.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetics/vendors/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assetics/vendors/morrisjs/morris.css">
	<script src="<?php echo base_url()?>assetics/js/jquery-2.1.4.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo base_url()?>assetics/vendors/bootstrap-3.3.5-dist/js/bootstrap.min.js" type="text/javascript" charset="utf-8" ></script>
	<script type="text/javascript" charset="utf-8">
		var domain = "<?php echo base_url() ?>";
	</script>
</head>
<body>