    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Buscar...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Paginas<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo base_url()?>index.php/consejoInfo/view/quienesSomos">Informaci&oacute;n Consejo</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>index.php/consejoInfo/view/autoridades">Autoridades</a>
                        </li>
                        <!--<li>
                            <a href="<?php echo base_url()?>index.php/consejoInfo/view/autoridades">Otras Secciones</a>
                        </li>-->
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="forms.html"><i class="fa fa-edit fa-fw"></i>Institutos <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo base_url()?>index.php/institucionesInfo/view/newInstituto">Nueva Instituto</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>index.php/institucionesInfo/view/institutos">Listar Instituto</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-sitemap fa-fw"></i> Novedades <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Novedades Institucionales <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?php echo base_url()?>index.php/novedadesInfo/view/newNovedad">Nueva Novedad</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>index.php/novedadesInfo/view/listNovedad">Listar Novedades</a>
                                </li>
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Novedades Legislativas <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?php echo base_url()?>index.php/novedadesLegislativas/view/newNovedad">Nueva Novedad</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>index.php/novedadesLegislativas/view/listNovedad">Listar Novedades</a>
                                </li>
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Novedades Comision J.A <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?php echo base_url()?>index.php/jPInfo/view/newNovedad">Nueva Novedad</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>index.php/jPInfo/view/listNovedad">Listar Novedades</a>
                                </li>
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-sitemap fa-fw"></i> Cursos <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Cursos<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?php echo base_url()?>index.php/cursosInfo/view/newCurso">Nuevo Curso/a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>index.php/cursosInfo/view/listCursos">Listar Curso</a>
                                </li>
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Posgrados <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                               <li>
                                    <a href="<?php echo base_url()?>index.php/posgradosInfo/view/newCurso">Nuevo Posgrado</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>index.php/posgradosInfo/view/listCursos">Listar Posgrados</a>
                                </li>
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Conferencias <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?php echo base_url()?>index.php/conferenciasInfo/view/newCurso">Nueva Conferencia</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>index.php/conferenciasInfo/view/listCursos">Listar Conferencias</a>
                                </li>
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="forms.html"><i class="fa fa-edit fa-fw"></i>Links de Interes <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo base_url()?>index.php/linksInfo/view/listLinks">Links de Interes</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>index.php/linksInfo/view/listPublicacion">Publicaciones</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
