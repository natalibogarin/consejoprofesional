<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminPanel extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->is_logged_in();
	}

	public function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			$this->load->view('adminPanel/login');
		}
	}

	public function index()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(isset($is_logged_in) || $is_logged_in == true)
		{
			$this->load->view('templates/adminPanel/head');
			$this->load->view('templates/adminPanel/header');
			$this->load->view('templates/adminPanel/menu');
			$this->load->view('adminPanel/home');
			$this->load->view('templates/adminPanel/footer');
		}
	}

	public function loggin()
	{
		$member = array(
					'username' => $this->input->post("username"), 
					'pass' => $this->input->post("pass"),
				);
		//$query = $this->login_model->validate($member);
		
		if(($member['username'] === "adminConsejo" && $member['pass'] === 'C0ns3j0Pr0f3s10n4l'))
		{
			$data = array(
				'username' => $this->input->post('username'),
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);
			//$this->login_model->saveActivity();
			redirect(base_url().'index.php/adminPanel/');
		} else {
			redirect(base_url()).'index.php/adminPanel/';
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url()).'index.php/adminPanel/';
	}
}

/* End of file AdminPanel.php */
/* Location: ./application/controllers/AdminPanel.php */