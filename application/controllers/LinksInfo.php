<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LinksInfo extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->is_logged_in();
	}

	public function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			redirect(base_url().'index.php/adminPanel','refresh');
		}
	}

	public function index()
	{

	}

	public function view($page='', $param1='')
	{
		$config['base_url'] = base_url()."index.php/linksInfo/view/".$page;
	    $config['per_page'] = 10;
	    $config['num_links']= 30;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['next_link'] = 'Siguiente';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['prev_link'] = 'Anterior';
	    $config ['uri_segment'] = 4;
		switch ($page) {
			case 'listLinks':
				$config['total_rows'] = $this->links_model->getTotalRows();
	    		$this->pagination->initialize($config);
				//$data['autoridades'] = $this->autoridades_model->getAutoridadesPerPage($config['per_page'],$param1);
				$data["links"] = $this->links_model->getLinks(1, $config['per_page'],$param1);
				$data["pages"]= $this->pagination->create_links();
				break;
			case 'newLink':
				break;
			case 'editLink':
				$data["link"] = $this->links_model->getLink($param1)[0];
				break;
			case 'listPublicacion':
				$config['total_rows'] = $this->links_model->getTotalRows();
	    		$this->pagination->initialize($config);
				//$data['autoridades'] = $this->autoridades_model->getAutoridadesPerPage($config['per_page'],$param1);
				$data["links"] = $this->links_model->getLinks(2, $config['per_page'],$param1);
				$data["pages"]= $this->pagination->create_links();
				break;
			case 'newPublicacion':
				break;
			case 'editPublicacion':
				$data["link"] = $this->links_model->getLink($param1)[0];
				break;
			default:
				# code...
				break;
		}

		$data["status"] = $param1;
		$this->load->view('templates/adminPanel/head');
		$this->load->view('templates/adminPanel/header');
		$this->load->view('templates/adminPanel/menu');
		$this->load->view('adminPanel/links/'.$page, $data);
		$this->load->view('templates/adminPanel/footer');
	}

	public function newLink()
	{

		$where = $this->input->post("where");
		$link = array(
			"linknombre" => $this->input->post("linknombre"),
			"link" => $this->input->post("link"),
			"linkhtml" => "",
			"linkfecha" => date('Y-m-d'),
			"linkhora" => date('H:i'),
			"tlid" => (int)$this->input->post("tlid")
		);

		if($this->links_model->newLink($link)){
			redirect(base_url().'index.php/linksInfo/view/'.$where.'/success','refresh');
		} else {
			redirect(base_url().'index.php/linksInfo/view/'.$where.'/error','refresh');
		}
	}

	public function editLink()
	{
		$where = $this->input->post("where");
		$link = array(
			"linkid" => $this->input->post("linkid"),
			"linknombre" => $this->input->post("linknombre"),
			"link" => $this->input->post("link"),
			"linkhtml" => "",
			"linkfecha" => date('Y-m-d'),
			"linkhora" => date('H:i'),
			"tlid" => (int)$this->input->post("tlid")
		);

		if($this->links_model->updateLink($link)){
			redirect(base_url().'index.php/linksInfo/view/'.$where.'/success','refresh');
		} else {
			redirect(base_url().'index.php/linksInfo/view/'.$where.'/error','refresh');
		}
	}

	public function deleteLink($linkid, $where)
	{
		if($this->links_model->deleteLink($linkid)){
			redirect(base_url().'index.php/linksInfo/view/'.$where.'/success','refresh');
		} else {
			redirect(base_url().'index.php/linksInfo/view/'.$where.'/error','refresh');
		}
	}

}

/* End of file LinksInfo.php */
/* Location: ./application/controllers/LinksInfo.php */