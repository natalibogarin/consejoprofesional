<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConsejoInfo extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->is_logged_in();
	}

	public function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			redirect(base_url().'index.php/adminPanel','refresh');
		}
	}

	public function index()
	{
		
	}

	public function view($page="", $param1="", $status2="")
	{
		$config['base_url'] = base_url()."index.php/consejoInfo/view/".$page;
	    $config['per_page'] = 10;
	    $config['num_links']= 30;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['next_link'] = 'Siguiente';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['prev_link'] = 'Anterior';
	    $config ['uri_segment'] = 4;

		switch ($page) {
			case 'autoridades':
				$config['total_rows'] = $this->autoridades_model->getTotalRows();
	    		$this->pagination->initialize($config);
				$data['autoridades'] = $this->autoridades_model->getAutoridadesPerPage($config['per_page'],$param1);
			    $data["pages"]= $this->pagination->create_links();
				break;
			case 'newAutoridad':
				$data["cargos"] = $this->autoridades_model->getCargos();
				break;
			case 'editAutoridad':
				$data["autoridad"] = $this->autoridades_model->getAutoridad($param1)[0];
				$data["cargos"] = $this->autoridades_model->getCargos();
				break;
			case 'quienesSomos':
				$data["contenido"] = $this->consejo_model->getSeccion("quienesSomos");
				$data["consejoId"] = ($data["contenido"]) ? $data["contenido"][0]->consejoId : 0 ;
				break;
			default:
				break;
		}

		$data["status"] = $param1;
		$this->load->view('templates/adminPanel/head');
		$this->load->view('templates/adminPanel/header');
		$this->load->view('templates/adminPanel/menu');
		$this->load->view('adminPanel/consejoInfo/'.$page, $data);
		$this->load->view('templates/adminPanel/footer');
	}

	public function saveQuienesSomos()
	{
		$quienesSomosInfo = array(
				'html' => $this->input->post("html"),
				'contenido' => $this->input->post("html"),
				'seccion' => $this->input->post("seccion"),
				'extra' => $this->input->post("extra")
			);
		if ($this->input->post("consejoId") != 0) {
			$quienesSomosInfo['consejoid'] = (int)$this->input->post("consejoId");
			$this->consejo_model->updateConsejoInfo($quienesSomosInfo);
			$transaccion = (int)$this->input->post("consejoId");
		} else {
			$transaccion = $this->consejo_model->newConsejoInfo($quienesSomosInfo);
		}
		echo json_encode(array('transaccion' => $transaccion ));
	}

	public function newAutoridad()
	{
		$nuevaAutoridad = array('profesionalnombre' => $this->input->post("profesionalnombre"),
							'profesionaltitulo' => $this->input->post("profesionaltitulo"),
							'cargoid' => $this->input->post("cargo")
		 				);

		if($this->autoridades_model->newAutoridad($nuevaAutoridad)){
			redirect(base_url().'index.php/consejoInfo/view/autoridades/success','refresh');
		} else {
			redirect(base_url().'index.php/consejoInfo/view/autoridades/error','refresh');
		}
	}

	public function updateAutoridad()
	{
		$autoridad = array(
							'profesionalid' => (int)$this->input->post("profesionalid"),
							'profesionalnombre' => $this->input->post("profesionalnombre"),
							'profesionaltitulo' => $this->input->post("profesionaltitulo"),
							'cargoid' => (int)$this->input->post("cargo")
		 				);

		if($this->autoridades_model->updateAutoridad($autoridad)){
			redirect(base_url().'index.php/consejoInfo/view/autoridades/success','refresh');
		} else {
			redirect(base_url().'index.php/consejoInfo/view/autoridades/error','refresh');
		}
	}

	public function deleteAutoridad($profesionalid)
	{
		if($this->autoridades_model->deleteAutoridad($profesionalid)){
			redirect(base_url().'index.php/consejoInfo/view/autoridades/success','refresh');
		} else {
			redirect(base_url().'index.php/consejoInfo/view/autoridades/error','refresh');
		}
	}

	public function upload_file() {
		$status = "";
		$msg = "";
		$file_element_name = 'novedadimg';

		if ($status != "error"){
			$config['upload_path'] = './assetics/img/';
			$config['allowed_types'] = 'gif|jpg|png|doc|txt';
			$config['max_size'] = 1024 * 8;
			$config['encrypt_name'] = FALSE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload($file_element_name))
			{
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			}else{
				$data = $this->upload->data();
				$image_path = $data['full_path'];
				if(file_exists($image_path)){
		  			$status = "success";
		  			$msg = base_url().'assetics/img/'.$data["file_name"];
				} else {
					$status = "error";
					$msg = "Something went wrong when saving the file, please try again.";
				}
			}
			@unlink($_FILES[$file_element_name]);
		}
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}

	public function newSeccion()
	{
		# code...
	}
}

/* End of file ConsejoInfo.php */
/* Location: ./application/controllers/ConsejoInfo.php */