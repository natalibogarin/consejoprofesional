<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JPInfo extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->is_logged_in();
	}

	public function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			redirect(base_url().'index.php/adminPanel','refresh');
		}
	}

	public function index()
	{
		
	}

	public function view($page='', $param1='')
	{
		$config['base_url'] = base_url()."index.php/jPInfo/view/".$page;
	    $config['per_page'] = 10;
	    $config['num_links']= 30;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['next_link'] = 'Siguiente';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['prev_link'] = 'Anterior';
	    $config ['uri_segment'] = 4;
		switch ($page) {
			case 'listNovedad':
				$config['total_rows'] = $this->novedades_model->getTotalRows();
	    		$this->pagination->initialize($config);
				//$data['autoridades'] = $this->autoridades_model->getAutoridadesPerPage($config['per_page'],$param1);
				$data["novedades"] = $this->novedades_model->getNovedades(1, $config['per_page'],$param1);
				$data["pages"]= $this->pagination->create_links();
				break;
			case 'newNovedad':
				break;
			case 'editNovedad':
				$data["novedad"] = $this->novedades_model->getNovedad($param1)[0];
				break;
			default:
				# code...
				break;
		}

		$data["status"] = $param1;
		$this->load->view('templates/adminPanel/head');
		$this->load->view('templates/adminPanel/header');
		$this->load->view('templates/adminPanel/menu');
		$this->load->view('adminPanel/jovenesProfesionales/'.$page, $data);
		$this->load->view('templates/adminPanel/footer');
	}

	public function newNovedad()
	{
		$nuevaNovedad = array(
			"novedadfecha" => date('Y-m-d'),
			"novedadresumen" => $this->input->post("novedadresumen"),
			"novedadcontenido" => $this->input->post("novedadcontenido"),
			"homeslide" => $this->input->post("homeslide"),
			"novedadtitulo" => $this->input->post("novedadtitulo"),
			"novedadimg" => $this->input->post("novedadimghidden"),
			"novedadhora" => date('H:i:s'),
			"tnid" => 1
		);

		if($this->novedades_model->newNovedad($nuevaNovedad)){
			$result = array('status' => 200, 
						'msg' => "Transacción Exitosa", 
						'action'=>base_url().'index.php/jPInfo/view/newNovedad/success');
		} else {
			$result = array('status' => 500, 
							'msg' => "Transacción Fallida", 
							'action'=>base_url().'index.php/jPInfo/view/newNovedad/error');
		}

		echo json_encode($result);
	}

	public function editNovedad()
	{
		$novedad = array(
			"novedadid" => $this->input->post("novedadid"),
			"novedadfecha" => date('Y-m-d'),
			"novedadresumen" => $this->input->post("novedadresumen"),
			"homeslide" => $this->input->post("homeslide"),
			"novedadcontenido" => $this->input->post("novedadcontenido"),
			"novedadtitulo" => $this->input->post("novedadtitulo"),
			"novedadimg" => $this->input->post("novedadimghidden"),
			"novedadhora" => date('H:i:s'),
			"tnid" => 1
		);

		if($this->novedades_model->updateNovedad($novedad)){
			$result = array('status' => 200, 
							'msg' => "Transacción Exitosa", 
							'action'=>base_url().'index.php/jPInfo/view/listNovedad/success');
		} else {
			$result = array('status' => 500, 
							'msg' => "Transacción Fallida", 
							'action'=>base_url().'index.php/jPInfo/view/listNovedad/error');
		}

		echo json_encode($result);
	}

	public function deleteNovedad($novedadid)
	{
		if($this->novedades_model->deleteNovedad($novedadid)){
			redirect(base_url().'index.php/jPInfo/view/listNovedad/success','refresh');
		} else {
			redirect(base_url().'index.php/jPInfo/view/listNovedad/error','refresh');
		}
	}
	public function upload_file() {
		$status = "";
		$msg = "";
		$file_element_name = 'novedadimg';

		if ($status != "error"){
			$config['upload_path'] = './assetics/img/jovenesprofesionales/';
			$config['allowed_types'] = 'gif|jpg|png|doc|txt';
			$config['max_size'] = 1024 * 8;
			$config['encrypt_name'] = FALSE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload($file_element_name))
			{
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			}else{
				$data = $this->upload->data();
				$image_path = $data['full_path'];
				if(file_exists($image_path)){
		  			$status = "success";
		  			$msg = base_url().'assetics/img/jovenesprofesionales/'.$data["file_name"];
				} else {
					$status = "error";
					$msg = "Something went wrong when saving the file, please try again.";
				}
			}
			@unlink($_FILES[$file_element_name]);
		}
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}
}

/* End of file jPInfo.php */
/* Location: ./application/controllers/JPInfo.php */