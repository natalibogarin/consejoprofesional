<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class InstitucionesInfo extends CI_Controller {

		public function __construct(){
			parent::__construct();
			$this->is_logged_in();
		}

		public function is_logged_in()
		{
			$is_logged_in = $this->session->userdata('is_logged_in');
			if(!isset($is_logged_in) || $is_logged_in != true)
			{
				redirect(base_url().'index.php/adminPanel','refresh');
			}
		}

		public function index()
		{
			
		}

		public function view($page='', $param1="")
		{
			$config['base_url'] = base_url()."index.php/institucionesInfo/view/".$page;
		    $config['per_page'] = 10;
		    $config['num_links']= 30;
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a>';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';
		    $config['next_link'] = 'Siguiente';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['prev_link'] = 'Anterior';
		    $config ['uri_segment'] = 4;

			switch ($page) {
				case 'institutos':
					$config['total_rows'] = $this->institutos_model->getTotalRows();
		    		$this->pagination->initialize($config);
					$data['institutos'] = $this->institutos_model->getInstitutosPerPage($config['per_page'],$param1);
				    $data["pages"]= $this->pagination->create_links();
					break;
				case 'newInstitucion':
					break;
				case 'editInstituto':
					$data["instituto"] = $this->institutos_model->getInstituto($param1)[0];
					break;
				default:
					break;
			}

			$data["status"] = $param1;
			$this->load->view('templates/adminPanel/head');
			$this->load->view('templates/adminPanel/header');
			$this->load->view('templates/adminPanel/menu');
			$this->load->view('adminPanel/institutosInfo/'.$page, $data);
			$this->load->view('templates/adminPanel/footer');
		}

		public function newInstituto()
		{
			$instituto = array(
				"institutonombre" => $this->input->post("institutonombre"),
				"institutoobjetivo" => $this->input->post("institutoobjetivo"),
				"institutomail" => $this->input->post("institutomail"),
				"institutofbFanpage" => $this->input->post("institutofbFanpage"),
				"institutotwFanpage" => $this->input->post("institutotwFanpage"),
				"institutologo" => $this->input->post("institutologo"),
				);
			if ($this->institutos_model->newInstituto($instituto)) {
				redirect(base_url().'index.php/institucionesInfo/view/newInstituto/success','refresh');
			} else {
				redirect(base_url().'index.php/institucionesInfo/view/newInstituto/error','refresh');
			}
		}

		public function deleteInstituto($institutoid)
		{
			if ($this->institutos_model->deleteInstituto($institutoid)) {
				redirect(base_url().'index.php/institucionesInfo/view/institutos/success','refresh');
			} else {
				redirect(base_url().'index.php/institucionesInfo/view/institutos/error','refresh');
			}
		}

		public function updateInstituto()
		{
			$instituto = array(
				"institutoId" => $this->input->post("institutoId"),
				"institutonombre" => $this->input->post("institutonombre"),
				"institutoobjetivo" => $this->input->post("institutoobjetivo"),
				"institutomail" => $this->input->post("institutomail"),
				"institutofbFanpage" => $this->input->post("institutofbFanpage"),
				"institutotwFanpage" => $this->input->post("institutotwFanpage"),
				"institutologo" => $this->input->post("institutologo"),
				);
			if ($this->institutos_model->updateInstituto($instituto)) {
				redirect(base_url().'index.php/institucionesInfo/view/institutos/success','refresh');
			} else {
				redirect(base_url().'index.php/institucionesInfo/view/institutos/error','refresh');
			}
		}
	}
/* End of file InstitucionesInfo.php */
/* Location: ./application/controllers/InstitucionesInfo.php */