<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$data["slider"] = $this->novedades_model->getNovedadesSlide(2);
		$data["novedades"] = $this->novedades_model->getNovedadesHome(2);
		$this->load->view('templates/head');
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('sitio/home', $data);
		$this->load->view('templates/aside');
		$this->load->view('templates/footer');
	}

	public function page($page='home',$param1="", $param2=""){
		$config['base_url'] = base_url()."index.php/sitio/page/".$page;
	    $config['per_page'] = 9;
	    $config['num_links']= 30;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['next_link'] = 'Siguiente';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['prev_link'] = 'Anterior';
	    $config ['uri_segment'] = 4;
		$data["param1"] = $param1;
		switch ($page) {
			case 'home':
				$data["slider"] = $this->novedades_model->getNovedadesSlide(2);
				$data["novedades"] = $this->novedades_model->getNovedades(2,10,0);
				break;
			case 'quienesomos':
				$data["contenido"] = $this->consejo_model->getSeccion("quienesSomos")[0];
				break;
			case 'detalle-novedades':
				$data["novedad"] = $this->novedades_model->getNovedadSlug($param1)[0];
				break;
			case 'novedades':
				$config['total_rows'] = $this->novedades_model->getTotalRows(2);
	    		$this->pagination->initialize($config);
				$data["novedades"] = $this->novedades_model->getNovedades(2, $config['per_page'],$param1);
				$data["pages"]= $this->pagination->create_links();
				break;
			case 'novedades-legislativas':
			$config['total_rows'] = $this->novedades_model->getTotalRows(3);
	    		$this->pagination->initialize($config);
				$data["novedades"] = $this->novedades_model->getNovedades(3, $config['per_page'],$param1);
				$data["pages"]= $this->pagination->create_links();
				break;
			case 'comisionjovenes':
				$config['total_rows'] = $this->novedades_model->getTotalRows(1);
	    		$this->pagination->initialize($config);
				$data["novedades"] = $this->novedades_model->getNovedades(1, $config['per_page'],$param1);
				$data["pages"]= $this->pagination->create_links();
				break;
			case 'institutos':
				$data['institutos'] = $this->institutos_model->getInstitutosPerPage(10,0);
				break;
			default:
				# code...
				break;
		}
		$this->load->view('templates/head');
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('sitio/'.$page, $data);
		$this->load->view('templates/aside');
		$this->load->view('templates/footer');
	}

	public function sendEmail()
	{
		if($this->input->post("nombre") && $this->input->post("mail") && $this->input->post("mensaje")){
			$to = "cpa@consejodeabogados.org.ar";
			$subject = $this->input->post("asunto");
			$contenido = "Nombre: ".$this->input->post("nombre")."\n";
			$contenido .= "Email: ".$this->input->post("mail")."\n\n";
			$contenido .= "Comentario: ".$this->input->post("mensaje")."\n\n";
			$header = "From: no-reply@consejodeabogados.org.ar\nReply-To:".$this->input->post("mail")."\n";
			$header .= "Mime-Version: 1.0\n";
			$header .= "Content-Type: text/plain";
			if(mail($to, $subject, $contenido ,$header)){
				redirect(base_url()."index.php/sitio/page/contacto/success",'refresh');
			} else {
				redirect(base_url()."index.php/sitio/page/contacto/error",'refresh');
			}
		}
	}
}
