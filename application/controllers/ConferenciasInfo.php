<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConferenciasInfo extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->is_logged_in();
	}

	public function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			redirect(base_url().'index.php/adminPanel','refresh');
		}
	}
	
	public function index()
	{

	}

	public function view($page='', $param1='')
	{
		$config['base_url'] = base_url()."index.php/conferenciasInfo/view/".$page;
	    $config['per_page'] = 10;
	    $config['num_links']= 30;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['next_link'] = 'Siguiente';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['prev_link'] = 'Anterior';
	    $config ['uri_segment'] = 4;
		switch ($page) {
			case 'listCursos':
				$config['total_rows'] = $this->cursos_model->getTotalRows();
	    		$this->pagination->initialize($config);
				//$data['autoridades'] = $this->autoridades_model->getAutoridadesPerPage($config['per_page'],$param1);
				$data["cursos"] = $this->cursos_model->getCursos(3, $config['per_page'],$param1);
				$data["pages"]= $this->pagination->create_links();
				break;
			case 'newCurso':
				break;
			case 'editCurso':
				$data["curso"] = $this->cursos_model->getCurso($param1)[0];
				break;
			default:
				# code...
				break;
		}

		$data["status"] = $param1;
		$this->load->view('templates/adminPanel/head');
		$this->load->view('templates/adminPanel/header');
		$this->load->view('templates/adminPanel/menu');
		$this->load->view('adminPanel/conferencias/'.$page, $data);
		$this->load->view('templates/adminPanel/footer');
	}

	public function newCurso()
	{

		$nuevoCurso = array(
			"cursoNombre" => $this->input->post("cursoNombre"),
			"cursoduracion" => $this->input->post("cursoduracion"),
			"cursodonde" => $this->input->post("cursodonde"),
			"cursodetalle" => $this->input->post("cursodetalle"),
			"cursoprograma" => $this->input->post("cursoprograma"),
			"cursolink" => $this->input->post("cursolink"),
			"tcid" => 3
		);

		if($this->cursos_model->newCurso($nuevoCurso)){
			$result = array('status' => 200, 
						'msg' => "Transacción Exitosa", 
						'action'=>base_url().'index.php/conferenciasInfo/view/newCurso/success');
		} else {
			$result = array('status' => 500, 
							'msg' => "Transacción Fallida", 
							'action'=>base_url().'index.php/conferenciasInfo/view/newCurso/error');
		}

		echo json_encode($result);
	}

	public function editCurso()
	{
		$curso = array(
			"cursoNombre" => $this->input->post("cursoNombre"),
			"cursoduracion" => $this->input->post("cursoduracion"),
			"cursodonde" => $this->input->post("cursodonde"),
			"cursodetalle" => $this->input->post("cursodetalle"),
			"cursoprograma" => $this->input->post("cursoprograma"),
			"cursolink" => $this->input->post("cursolink"),
			"tcid" => 3,
			"cursoId" => $this->input->post("cursoId")
		);

		if($this->cursos_model->updateCurso($curso)){
			$result = array('status' => 200, 
							'msg' => "Transacción Exitosa", 
							'action'=>base_url().'index.php/conferenciasInfo/view/listCursos/success');
		} else {
			$result = array('status' => 500, 
							'msg' => "Transacción Fallida", 
							'action'=>base_url().'index.php/conferenciasInfo/view/listCursos/error');
		}

		echo json_encode($result);
	}

	public function deleteCurso($cursoId)
	{
		if($this->cursos_model->deleteCurso($cursoId)){
			redirect(base_url().'index.php/conferenciasInfo/view/listCursos/success','refresh');
		} else {
			redirect(base_url().'index.php/conferenciasInfo/view/listCursos/error','refresh');
		}
	}

	public function upload_file() {
		$status = "";
		$msg = "";
		$file_element_name = 'cursolink';

		if ($status != "error"){
			$config['upload_path'] = './assetics/img/conferencias/';
			$config['allowed_types'] = 'gif|jpg|png|doc|txt';
			$config['max_size'] = 1024 * 8;
			$config['encrypt_name'] = FALSE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload($file_element_name))
			{
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			}else{
				$data = $this->upload->data();
				$image_path = $data['full_path'];
				if(file_exists($image_path)){
		  			$status = "success";
		  			$msg = base_url().'assetics/img/conferencias/'.$data["file_name"];
				} else {
					$status = "error";
					$msg = "Something went wrong when saving the file, please try again.";
				}
			}
			@unlink($_FILES[$file_element_name]);
		}
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}
}

/* End of file ConferenciasInfo.php */
/* Location: ./application/controllers/ConferenciasInfo.php */